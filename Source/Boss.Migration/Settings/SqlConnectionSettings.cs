﻿using System;
using System.Collections.Generic;
using Boss.MigrationCore.Common.Exceptions;
using Boss.MigrationCore.Common.Serilizer;
using Newtonsoft.Json;

namespace Boss.Migration.Settings
{
    public class SqlConnectionSettings : MigrationCore.Common.Settings
    {
        public override Guid WorkflowId { get; set; } = Guid.Parse("{DED45518-13D2-4EF7-9A6E-F69EA7963B14}");
        private string _server;
        private string _database;
        private bool _isCorrect;
        private string _fileFullPath;

        [JsonIgnore]
        public bool IsValidated { get; set; } = true;

        [JsonIgnore]
        public bool IsCorrect
        {
            get { return _isCorrect; }
            set
            {
                _isCorrect = value;
                OnPropertyChanged();
            }
        }



        public string Server
        {
            get { return _server; }
            set
            {
                _server = value;
                OnPropertyChanged();
                Validate();
            }
        }

        public string Database
        {
            get { return _database; }
            set
            {
                _database = value;
                OnPropertyChanged();
                Validate();
            }
        }

        public string ConnectionString()
        {
            return $"Server={Server};Database={Database};Integrated Security=True;";
        }



        public SqlConnectionSettings(ISerilizer serilizer) : base(serilizer)
        {
        }

        public override OperationResult Validate()
        {
            IsCorrect = !IsValidated || (!string.IsNullOrEmpty(Server) && !string.IsNullOrEmpty(Database));
            return new OperationResult() { Result = IsCorrect, Errors = new List<ErrorString>() };
        }

        public override void Read(string data)
        {
            _fileFullPath = data;
            var jsonFileSerilizer = Serilizer as JsonFileSerilizer;
            if (jsonFileSerilizer != null)
            {
                var readData = jsonFileSerilizer.Read<SqlConnectionSettings>(data) ?? new SqlConnectionSettings(Serilizer);

                Server = readData.Server;
                Database = readData.Database;
            }
        }

        public override string Write()
        {
            string res = "";
            var jsonFileSerilizer = Serilizer as JsonFileSerilizer;
            if (jsonFileSerilizer != null && !string.IsNullOrEmpty(_fileFullPath))
            {
                res = jsonFileSerilizer.Write(_fileFullPath, this);
            }
            return res;
        }

    }
}
