﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Boss.BaseControls.Models;
using Boss.Migration.Context;
using Boss.Migration.Enumerators;
using Boss.Migration.Http.Model;
using Boss.Migration.Http.Request;
using Boss.Migration.Logging;
using Boss.Migration.Models;
using Boss.MigrationCore.Common.Exceptions;
using Boss.MigrationCore.Common.Serilizer;
using Boss.MigrationCore.Logging;
using Newtonsoft.Json;
using RestSharp;

namespace Boss.Migration.Settings
{

    public class MigrationSettings : MigrationCore.Common.Settings
    {

        public override Guid WorkflowId { get; set; } = Guid.Parse("{4D057FCF-D51F-42C7-9569-F2331DBB7BEC}");
        private string _fileFullPath;

        private RangeEnabledObservableCollection<CheckGuidWithRouteModel> _migrationModules = new RangeEnabledObservableCollection<CheckGuidWithRouteModel>();

        private Dictionary<CategoriesTransformType, string> _categoriesTransformCollection = new Dictionary<CategoriesTransformType, string>()
        {
            { CategoriesTransformType.AllDash, "Group->Category->Subcategory  -> Group - Category - Subcategory"},
            { CategoriesTransformType.AllSpace, "Group->Category->Subcategory  -> Group Category Subcategory"},
            { CategoriesTransformType.AllUnderline, "Group->Category->Subcategory  -> Group_Category_Subcategory"},
            { CategoriesTransformType.GroupAndCategoryAsCustomField, "Group->Category->Subcategory  -> Name: Group, Field: Category"},
            { CategoriesTransformType.GroupAndCategoryAsTag, "Group->Category->Subcategory  -> Name: Group, Tag: Category"},
            { CategoriesTransformType.GroupOnly, "Group->Category->Subcategory  -> Group"},
        };

        private string _bossDeskUrl;
        private bool _ticketSelectorIsEnabled;
        private DateTime _ticketSelectorDate = DateTime.Now;
        private CategoriesTransformType _currentCategoriesTransformType = CategoriesTransformType.GroupOnly;
        private string _apiKey;
        private bool _removePreviousData;

        public RangeEnabledObservableCollection<CheckGuidWithRouteModel> MigrationModules
        {
            get { return _migrationModules; }
            set
            {
                _migrationModules = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public Dictionary<CategoriesTransformType, string> CategoriesTransformCollection
        {
            get { return _categoriesTransformCollection; }
            set
            {
                _categoriesTransformCollection = value;
                OnPropertyChanged();
            }
        }

        public CategoriesTransformType CurrentCategoriesTransformType
        {
            get { return _currentCategoriesTransformType; }
            set
            {
                _currentCategoriesTransformType = value;
                OnPropertyChanged();
            }
        }

        public string ApiKey
        {
            get { return _apiKey; }
            set
            {
                _apiKey = value;
                OnPropertyChanged();
            }
        }

        public string BossDeskUrl
        {
            get { return _bossDeskUrl; }
            set
            {
                _bossDeskUrl = value;
                OnPropertyChanged();
            }
        }

        public bool TicketSelectorIsEnabled
        {
            get { return _ticketSelectorIsEnabled; }
            set
            {
                _ticketSelectorIsEnabled = value;
                OnPropertyChanged();
            }
        }

        public DateTime TicketSelectorDate
        {
            get { return _ticketSelectorDate; }
            set
            {
                _ticketSelectorDate = value;
                OnPropertyChanged();
            }
        }

        public bool RemovePreviousData
        {
            get { return _removePreviousData; }
            set
            {
                _removePreviousData = value;
                OnPropertyChanged();
            }
        }

        public MigrationSettings(ISerilizer serilizer) : base(serilizer)
        {
            //string a = "ssdfsd";

            //char[] s =new []{'x','c'};
            //var sas = a.Split(new string[] {"sadasd"}, StringSplitOptions.None);
        }

        public override OperationResult Validate()
        {
            var result = new OperationResult();

            if (MigrationModules == null || MigrationModules.Count == 0)
            {
                result.Errors.Add(new ErrorString()
                {
                    SubStrings = new List<ErrorSubString>()
                    {
                        new ErrorSubString(ErrorSubStringType.Caption, "Invalid"),
                        new ErrorSubString(ErrorSubStringType.Object, "Data Migration Modules")
                    }
                });
            }
            else if (!MigrationModules.Any(x => x.IsEnabled))
            {
                result.Errors.Add(new ErrorString()
                {
                    SubStrings = new List<ErrorSubString>()
                    {
                        new ErrorSubString(ErrorSubStringType.Caption, "Please select"),
                        new ErrorSubString(ErrorSubStringType.Object, "Data Migration Modules")
                    }
                });
            }

            if (string.IsNullOrEmpty(ApiKey))
                result.Errors.Add(new ErrorString()
                {
                    SubStrings = new List<ErrorSubString>()
                        {
                            new ErrorSubString(ErrorSubStringType.Caption, "Please enter"),
                            new ErrorSubString(ErrorSubStringType.Object, "Api Key")
                        }
                });

            if (string.IsNullOrEmpty(BossDeskUrl))
                result.Errors.Add(new ErrorString()
                {
                    SubStrings = new List<ErrorSubString>()
                        {
                            new ErrorSubString(ErrorSubStringType.Caption, "Invalid"),
                            new ErrorSubString(ErrorSubStringType.Object, "BOSSDesk URL")
                        }
                });

            if (!result.Errors.Any())
            {
                //todo api key check
                //todo connect validation
                DefaultRequest request = new DefaultRequest(this, new Logger(Constants.CurrentMigrationTarget), new JsonFileSerilizer())
                {
                    Route = "heartbeat"
                };
                var r = request.Exeсute(Method.GET, "") as List<HttpResult>;
            }



            result.Result = !result.Errors.Any();

            return result;
        }

        public override void Read(string data)
        {
            _fileFullPath = data;
            var jsonFileSerilizer = Serilizer as JsonFileSerilizer;
            if (jsonFileSerilizer != null)
            {
                bool settingsNotFound = jsonFileSerilizer.Read<MigrationSettings>(data) == null;
                var readData = jsonFileSerilizer.Read<MigrationSettings>(data) ?? new MigrationSettings(Serilizer);

                MigrationModules = readData.MigrationModules;
                CategoriesTransformCollection = readData.CategoriesTransformCollection;
                CurrentCategoriesTransformType = readData.CurrentCategoriesTransformType;
                ApiKey = readData.ApiKey;
                BossDeskUrl = readData.BossDeskUrl;
                TicketSelectorIsEnabled = readData.TicketSelectorIsEnabled;
                TicketSelectorDate = readData.TicketSelectorDate;

                if (MigrationModules == null || MigrationModules.Count == 0)
                {
                    try
                    {
                        var projectSettings = new MigrationProjectsSettings(new JsonFileSerilizer());
                        projectSettings.Read(Path.Combine(new BaseWorkContext(Constants.CurrentMigrationTarget).SettingsDirectory, $"{projectSettings.WorkflowId}.json"));
                        var currentProjectName = projectSettings.LastProject.Value.ToString();
                        if (Constants.DefaultsCollectionsForProjects[Constants.CurrentMigrationTarget].ContainsKey(currentProjectName))
                        {
                            MigrationModules = new RangeEnabledObservableCollection<CheckGuidWithRouteModel>(Constants.DefaultsCollectionsForProjects[Constants.CurrentMigrationTarget][currentProjectName]);
                        }
                    }
                    catch 
                    {
                        //todo 
                    }
                }

                if (settingsNotFound) Write();
            }
        }

        public override string Write()
        {
            string res = "";

            if (TicketSelectorIsEnabled) TicketSelectorDate = new DateTime(TicketSelectorDate.Year, TicketSelectorDate.Month, TicketSelectorDate.Day);

            var jsonFileSerilizer = Serilizer as JsonFileSerilizer;
            if (jsonFileSerilizer != null && !string.IsNullOrEmpty(_fileFullPath))
            {
                res = jsonFileSerilizer.Write(_fileFullPath, this);
            }
            return res;
        }
    }

}



