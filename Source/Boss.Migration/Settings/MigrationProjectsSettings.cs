﻿using System;
using System.Collections.Generic;
using System.Linq;
using Boss.BaseControls.Models;
using Boss.Migration.Enumerators;
using Boss.Migration.Http.Model;
using Boss.Migration.Http.Request;
using Boss.Migration.Models;
using Boss.MigrationCore.Common.Exceptions;
using Boss.MigrationCore.Common.Serilizer;
using Boss.MigrationCore.Logging;
using Newtonsoft.Json;
using RestSharp;

namespace Boss.Migration.Settings
{

    public class MigrationProjectsSettings : MigrationCore.Common.Settings
    {
        public override Guid WorkflowId { get; set; } = Guid.Parse("{BF8F2654-234C-49A5-A218-D69B8D72FEFB}");
        private string _fileFullPath;

        public RangeEnabledObservableCollection<KeyValue> Projects { get; set; } = new RangeEnabledObservableCollection<KeyValue>();

        public KeyValue LastProject { get; set; }

        public MigrationProjectsSettings(ISerilizer serilizer) : base(serilizer)
        {
        }

        public override OperationResult Validate()
        {
            var result = new OperationResult();

            if (Projects == null || Projects.Count == 0)
            {
                result.Errors.Add(new ErrorString()
                {
                    SubStrings = new List<ErrorSubString>()
                    {
                        new ErrorSubString(ErrorSubStringType.Caption, "Invalid"),
                        new ErrorSubString(ErrorSubStringType.Object, "Data Migration Projects Settings")
                    }
                });
            }

            result.Result = !result.Errors.Any();

            return result;
        }

        public override void Read(string data)
        {
            _fileFullPath = data;
            var jsonFileSerilizer = Serilizer as JsonFileSerilizer;
            if (jsonFileSerilizer != null)
            {
                bool settingsNotFound = jsonFileSerilizer.Read<MigrationProjectsSettings>(data) == null;
                var readData = jsonFileSerilizer.Read<MigrationProjectsSettings>(data) ?? new MigrationProjectsSettings(Serilizer);
                Projects = readData.Projects != null && readData.Projects.Any() ? readData.Projects : Constants.DefaultMigrationConstantConfigs[Constants.CurrentMigrationTarget].DefaultProject;
                var last = readData.Projects?.FirstOrDefault(x => x.Key.ToString() == readData.LastProject.Key.ToString()) ?? Projects.FirstOrDefault();
                LastProject = last;
                if (settingsNotFound) Write();
            }
        }

        public override string Write()
        {
            string res = "";
            var jsonFileSerilizer = Serilizer as JsonFileSerilizer;
            if (jsonFileSerilizer != null && !string.IsNullOrEmpty(_fileFullPath))
            {
                res = jsonFileSerilizer.Write(_fileFullPath, this);
            }
            return res;
        }
    }
}



