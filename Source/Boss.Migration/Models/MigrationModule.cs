﻿using System;
using System.Collections.Generic;
using Boss.BaseControls.Models;
using Newtonsoft.Json;

namespace Boss.Migration.Models
{
    public class MigrationModule : CheckModel
    {
        private string _query;
        private RangeEnabledObservableCollection<ModuleField> _fields;
        private bool _useDateSelector;
        private string _dateSelectorQuery = "where DATEDIFF(day, CAST('{D}' AS DATETIME), CreatedAtField) >= 0";

        [JsonIgnore]
        public Dictionary<Type, string> DefaultResponseTypes { get; set; } = new Dictionary<Type, string>()
        {
            {typeof(int), "int" },
            {typeof(long), "long" },
            {typeof(Guid), "guid" },
            {typeof(object), "object" },
            {typeof(string), "string" },
            {typeof(DateTime), "datetime" },
            {typeof(bool), "bool" },
            {typeof(double), "double" },
        }; 

        public Guid Guid { get; set; }

        public string Query
        {
            get { return _query; }
            set
            {
                _query = value;
                OnPropertyChanged();
            }
        }

        public RangeEnabledObservableCollection<ModuleField> Fields
        {
            get { return _fields; }
            set
            {
                _fields = value;
                OnPropertyChanged();
            }
        }

        public bool UseDateSelector
        {
            get { return _useDateSelector; }
            set
            {
                _useDateSelector = value;
                OnPropertyChanged();
            }
        }

        public string DateSelectorQuery
        {
            get { return _dateSelectorQuery; }
            set
            {
                _dateSelectorQuery = value;
                OnPropertyChanged();
            }
        }

        public string Route { get; set; }

        public Type ExternalId { get; set; } = typeof (int);

        public Type InternalId { get; set; } = typeof (int);

        public bool IsResponseWithResult { get; set; } = true;
    }
}
