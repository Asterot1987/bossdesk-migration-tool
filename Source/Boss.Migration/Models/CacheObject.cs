﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Boss.Migration.Http.Model;

namespace Boss.Migration.Models
{
    public class CacheObject
    {
        public Guid ModuleGuid { get; set; }

        public List<HttpResult> Objects { get; set; }

        public CacheObject()
        {
            Objects = new List<HttpResult>();
        }
    }
}
