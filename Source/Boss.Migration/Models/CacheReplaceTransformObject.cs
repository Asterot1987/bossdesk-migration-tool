﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Boss.BaseControls.Models;

namespace Boss.Migration.Models
{
    public class CacheReplaceTransformObject : BaseModel
    {
        private Guid _moduleGuid;
        private Guid _fieldGuid;

        public Guid ModuleGuid
        {
            get { return _moduleGuid; }
            set
            {
                _moduleGuid = value;
                OnPropertyChanged();
            }
        }

        public Guid FieldGuid
        {
            get { return _fieldGuid; }
            set
            {
                _fieldGuid = value;
                OnPropertyChanged();
            }
        }
    }
}

