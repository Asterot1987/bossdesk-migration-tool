﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Boss.BaseControls.Models;
using Newtonsoft.Json;

namespace Boss.Migration.Models
{
    public enum TransformType
    {
        Default,
        HierarchicalTransform,
        DefaultReplaceWithCacheObject,
        HierarchicalTransformAndReplaceWithCacheObject,
        HashCode,
        UuidTransform,
        SplitTransform,
        SplitAndUuidTransform
    }

    public class ModuleField : BaseModel
    {
        private bool _isEnabled;
        private string _sourceName;
        private Type _sourceType;
        private string _targetName;
        private Type _targetType;
        private RangeEnabledObservableCollection<FieldRecord> _replaceRecords = new RangeEnabledObservableCollection<FieldRecord>();
        private TransformType _transformType;
        private Dictionary<TransformType, string> _transformTypeDictionary = new Dictionary<TransformType, string>()
        {
            { TransformType.Default, "Default" },
            { TransformType.HierarchicalTransform, "Hierarchical Transform" },
            { TransformType.DefaultReplaceWithCacheObject, "Default Replace With Cache Object" },
            { TransformType.HierarchicalTransformAndReplaceWithCacheObject, "Hierarchical Transform Replace With Cache Object" },
            { TransformType.HashCode, "Get Hash Code" },
            {TransformType.UuidTransform, "Uuid Transform" },
            {TransformType.SplitTransform, "Split Transform" },
            {TransformType.SplitAndUuidTransform, "Split and Uuid Transform" }
        };

        private string _transformTypeString;
        private bool _isCacheObject;
        private Guid _guid = Guid.NewGuid();
        private CacheReplaceTransformObject _replaceTransformObject = new CacheReplaceTransformObject();

        public Guid Guid
        {
            get { return _guid; }
            set
            {
                if (value != Guid.Empty) _guid = value;
            }
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                _isEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool IsCacheObject
        {
            get { return _isCacheObject; }
            set
            {
                _isCacheObject = value;
                OnPropertyChanged();
            }
        }

        public string SourceName
        {
            get { return _sourceName; }
            set
            {
                _sourceName = value;
                OnPropertyChanged();
            }
        }

        public Type SourceType
        {
            get { return _sourceType; }
            set
            {
                _sourceType = value;
                OnPropertyChanged();
            }
        }

        public string TargetName
        {
            get { return _targetName; }
            set
            {
                _targetName = value;
                OnPropertyChanged();
            }
        }

        public Type TargetType
        {
            get { return _targetType; }
            set
            {
                _targetType = value;
                OnPropertyChanged();
            }
        }

        public TransformType TransformType
        {
            get { return _transformType; }
            set
            {
                _transformType = value;
                TransformTypeString = "";
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string TransformTypeString
        {
            get { return TransformTypeDictionary[TransformType]; }
            set
            {
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public Dictionary<TransformType, string> TransformTypeDictionary
        {
            get { return _transformTypeDictionary; }
            set
            {
                _transformTypeDictionary = value;
                OnPropertyChanged();
            }
        }

        public RangeEnabledObservableCollection<FieldRecord> ReplaceRecords
        {
            get { return _replaceRecords; }
            set
            {
                _replaceRecords = value ?? new RangeEnabledObservableCollection<FieldRecord>();
                OnPropertyChanged();
            }
        }


        public CacheReplaceTransformObject ReplaceTransformObject
        {
            get { return _replaceTransformObject; }
            set
            {
                _replaceTransformObject = value; 
                OnPropertyChanged();
            }
        }

        public int SplitTransformIndex { get; set; }

        public string SplitTransformSeparator { get; set; } = "";

        public bool SplitSetNullIfNotFoundSeparator { get; set; }

    }
}
