﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Boss.BaseControls.Models;

namespace Boss.Migration.Models
{
    public class CheckGuidWithRouteModel : CheckGuidModel
    {
        public string Route { get; set; }

        public CheckGuidWithRouteModel()
        {
        }

        public CheckGuidWithRouteModel(string guid, string caption, string route, bool isEnabled)
        {
            Guid = Guid.Parse(guid);
            Caption = caption;
            Route = route;
            IsEnabled = isEnabled;
        }
    }
}
