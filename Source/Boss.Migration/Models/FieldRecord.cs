﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Boss.BaseControls.Models;

namespace Boss.Migration.Models
{
    public class FieldRecord : BaseModel
    {
        private string _sourceValue;
        private string _targetValue;

        public string SourceValue
        {
            get { return _sourceValue; }
            set
            {
                _sourceValue = value;
                OnPropertyChanged();
            }
        }

        public string TargetValue
        {
            get { return _targetValue; }
            set
            {
                _targetValue = value;
                OnPropertyChanged();
            }
        }
    }
}
