﻿using System;
using System.IO;
using System.Reflection;
using Boss.MigrationCore.AccessControl;
using Boss.MigrationCore.Logging;

namespace Boss.Migration.Helper
{
    public class DefaultModulesOperations
    {
        private ILog _logger { get; set; }

        public DefaultModulesOperations(ILog logger)
        {
            _logger = logger;
        }

        public string ReadModule(string resPath, string resName)
        {
            string result = "";
            try
            {
                var assembly = Assembly.GetExecutingAssembly();

                //string[] resourceNames = Assembly.GetExecutingAssembly().GetManifestResourceNames();

                var resourceName = resPath + resName;

                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                    if (stream != null)
                        using (StreamReader reader = new StreamReader(stream))
                        {
                            result = reader.ReadToEnd();
                        }
            }
            catch (Exception ex)
            {
                _logger.Error($"ReadModule [{resPath}{resName}] Error ", ex);
            }
            
            return result;
        }

        public bool WriteModule(string path, string fileName, string module)
        {
            bool result = false;
            try
            {
                if (!string.IsNullOrEmpty(module))
                {
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                        using (var sac = new SetAccessControl())
                        {
                            sac.SetDirectoryAccessControl(path);
                        }
                    }
                    File.WriteAllText(Path.Combine(path, fileName), module);
                    using (var sac = new SetAccessControl())
                    {
                        sac.SetFileAccessControl(Path.Combine(path, fileName));
                    }
                    result = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error($"WriteModule [{Path.Combine(path, fileName)}] Error ", ex);
            }
            return result;
        }
    }
}
