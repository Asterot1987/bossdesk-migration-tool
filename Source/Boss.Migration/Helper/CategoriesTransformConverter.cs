﻿using System;
using System.Collections.Generic;
using System.Linq;
using Boss.Migration.Enumerators;

namespace Boss.Migration.Helper
{
    public static class CategoriesTransformConverter
    {
        public static string ConvertName(CategoriesTransformType transformType, string source)
        {
            string target = "";

            switch (transformType)
            {
                case CategoriesTransformType.AllDash:
                    target = source.Replace("->", " - ");
                    break;
                case CategoriesTransformType.AllSpace:
                    target = source.Replace("->", " ");
                    break;
                case CategoriesTransformType.AllUnderline:
                    target = source.Replace("->", "_");
                    break;
                case CategoriesTransformType.GroupOnly:
                case CategoriesTransformType.GroupAndCategoryAsCustomField:
                case CategoriesTransformType.GroupAndCategoryAsTag:
                    string[] stringSeparators = new string[] { "->" };
                    target = source.Split(stringSeparators, StringSplitOptions.None).FirstOrDefault();
                    break;
            }
            return target;
        }

        public static KeyValuePair<string, object> ConvertAsAdditionalField(CategoriesTransformType transformType, string source)
        {
            KeyValuePair<string, object> result = new KeyValuePair<string, object>(string.Empty, null);

            string[] stringSeparators = new string[] { "->" };
            var arr = source.Split(stringSeparators, StringSplitOptions.None);

            if (arr.Length == 3)
            {
                switch (transformType)
                {
                    case CategoriesTransformType.GroupAndCategoryAsTag:
                        result = new KeyValuePair<string, object>(Constants.TicketCategoryAsTag, arr[1]);
                        break;
                    case CategoriesTransformType.GroupAndCategoryAsCustomField:
                        result = new KeyValuePair<string, object>(Constants.TicketCategoryAsCustomField, arr[1]);
                        break;
                }
            }

            return result;
        }
    }
}
