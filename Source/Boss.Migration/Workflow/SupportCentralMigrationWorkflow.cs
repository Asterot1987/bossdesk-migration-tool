﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using Boss.Migration.Context;
using Boss.Migration.Http.Model;
using Boss.Migration.Http.Request;
using Boss.Migration.Models;
using Boss.Migration.Settings;
using Boss.MigrationCore.Common.Serilizer;
using Boss.MigrationCore.Context;
using Boss.MigrationCore.Logging;
using NDatabase;
using Newtonsoft.Json;
using RestSharp;
using SupportCentral.WebApi.Client.Base;
using SupportCentral.WebApi.Client.Settings;
using SupportCentral.WebApi.Models.Base;
using SupportCentral.WebApi.Models.Users;

namespace Boss.Migration.Workflow
{
    public class SupportCentralMigrationWorkflow : BaseMigrationWorkflow
    {
        //public DefaultRequest Request { get; set; }
        private ISerilizer _serilizer;
        private string _route;

        public SupportCentralMigrationWorkflow(string connectionString, string route, MigrationSettings migrationSettings, MigrationModule migrationModule, IWorkContext workContext, ILog logger, ISerilizer serilizer) : base(connectionString, migrationSettings, migrationModule, workContext, logger)
        {
            _serilizer = serilizer;
            _route = route;
        }

        public T Cast<T>(object o)
        {
            return (T)o;
        }

        public override void Save(IEnumerable<ExpandoObject> items)
        {
            //for test
            try
            {
                JsonFileSerilizer js = new JsonFileSerilizer();
                var path = Path.Combine(WorkContext.DataDirectory, $"{MigrationModule.Caption}_{MigrationModule.Guid}.json");
                js.Write(path, items);
            }
            catch (System.Exception ex)
            {
                Logger.Error("Could not save data.", ex);
            }

            //sending
            bool isCorrect = true;
            List<HttpResult> results = new List<HttpResult>();

            var settings = new BscApiSettings()
            {
                BaseUrl = MigrationSettings.BossDeskUrl,
                ApiKey = MigrationSettings.ApiKey,
                RecordsPerPost = 100
            };

            if (DataCollection != null && DataCollection.Count > 0)
            {

                //DataCollection = new List<ExpandoObject>(DataCollection.Take(1));
                int take = 100;
                int skip = 0;

                Logger.Info($"Start sending of {DataCollection.Count} records");
                Logger.Info($"Request behavior: [{take}] records per request");


                do
                {

                    if (skip > 0) base.Logger.Info($"Sent [{skip}] from [{DataCollection.Count} records");
                    results = new List<HttpResult>();

                    IEnumerable<ExpandoObject> send = new List<ExpandoObject>(DataCollection.Skip(skip).Take(take).Select(x => x).ToList());

                    try
                    {
                        if (!MigrationModule.IsResponseWithResult)
                        {
                            var client = new JsonBscApiClient<int, int>(settings);
                            var response = client.ImportJsonListBase(_serilizer.Serilize(send), HttpMethod.Post, _route).Result as BaseResponse;

                            if (response == null)
                            {
                                results.Add(new HttpResult()
                                {
                                    errors = new Error(new Exception("HttpResult is null"))
                                });
                            }
                            else if (!response.Success)
                            {
                                results.Add(new HttpResult()
                                {
                                    errors = new Error(response.Error.Message)
                                });
                            }
                        }
                        else
                        {
                            Type client = typeof(JsonBscApiClient<,>);
                            Type[] typeArgs = { MigrationModule.ExternalId, MigrationModule.InternalId };
                            Type genericClient = client.MakeGenericType(typeArgs);

                            //constructor
                            ConstructorInfo clientConstructor = genericClient.GetConstructor(new Type[] { typeof(IBscApiSettings) });
                            object clientObject = clientConstructor.Invoke(new object[] { settings });

                            //method invoke
                            MethodInfo importMethod = genericClient.GetMethod("ImportJsonList");
                            var postResult = importMethod.Invoke(clientObject, new object[] { _serilizer.Serilize(send), HttpMethod.Post, _route });

                            var str = _serilizer.Serilize(postResult);
                            var baseResult = _serilizer.Deserilize<BaseResponse<List<IdPair<object, object>>>>(str);

                            if (baseResult == null)
                            {
                                results.Add(new HttpResult()
                                {
                                    errors = new Error(new Exception("HttpResult is null"))
                                });
                            }
                            else if (!baseResult.Success)
                            {
                                results.Add(new HttpResult()
                                {
                                    errors = new Error(baseResult.Error.Message)
                                });
                            }
                            else if (baseResult.Success)
                            {
                                results.AddRange(baseResult.Result.Select(x => new HttpResult()
                                {
                                    legacy_id = x.ExternalId,
                                    bossdesk_id = x.InternalId
                                }));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        results.Add(new HttpResult()
                        {
                            errors = new Error(ex)
                        });
                        isCorrect = false;
                    }

                    skip += take;

                } while (DataCollection.Count > skip);


                var exceptions = results.Where(x => x?.errors?.exception != null).Select(x => x.errors.exception);
                foreach (var exception in exceptions)
                {
                    WorkflowActionError?.Invoke("Sending error.", exception);
                }

                var cache =
                results.Where(
                    x => x != null && x.errors.exception == null && x.errors.name.All(string.IsNullOrEmpty));

                if (cache.Any())
                {
                    try
                    {
                        using (var odb = OdbFactory.Open(Path.Combine(WorkContext.BaseDirectory, dbFileName)))
                        {
                            var obj = odb.AsQueryable<CacheObject>()
                                .FirstOrDefault(x => x.ModuleGuid == MigrationModule.Guid);

                            if (obj != null) odb.Delete(obj);

                            odb.Store(new CacheObject()
                            {
                                ModuleGuid = MigrationModule.Guid,
                                Objects = new List<HttpResult>(cache)
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Cache saving error", ex);
                    }
                    
                }

                if (isCorrect) WorkflowActionSuccess?.Invoke("Data sending completed successfully");
                else WorkflowActionError?.Invoke("Sending data is completed with errors", null);

            }

        }



        //public override void Save(IEnumerable<ExpandoObject> items)
        //{
        //    //for test
        //    try
        //    {
        //        JsonFileSerilizer js = new JsonFileSerilizer();
        //        var path = Path.Combine(WorkContext.DataDirectory, $"{MigrationModule.Caption}_{MigrationModule.Guid}.json");
        //        js.Write(path, items);
        //    }
        //    catch (System.Exception ex)
        //    {
        //        Logger.Error("Could not save data.", ex);
        //    }

        //    //sending
        //    bool isCorrect = true;
        //    List<HttpResult> results = new List<HttpResult>();

        //    var settings = new BscApiSettings()
        //    {
        //        BaseUrl = MigrationSettings.BossDeskUrl,
        //        ApiKey = MigrationSettings.ApiKey,
        //        RecordsPerPost = 100
        //    };

        //    if (DataCollection != null && DataCollection.Count > 0)
        //    {

        //        DataCollection = new List<ExpandoObject>(DataCollection.Take(1));

        //        int take = 100;
        //        int skip = 0;

        //        Logger.Info($"Start sending of {DataCollection.Count} records");
        //        Logger.Info($"Request behavior: [{take}] records per request");

        //        try
        //        {
        //            do
        //            {

        //                if (skip > 0) base.Logger.Info($"Sent [{skip}] from [{DataCollection.Count} records");
        //                results = new List<HttpResult>();

        //                IEnumerable<ExpandoObject> send = new List<ExpandoObject>(DataCollection.Skip(skip).Take(take).Select(x => x).ToList());

        //                var client = new JsonBscApiClient(settings);
        //                Task continuation = client.ImportJsonList(_serilizer.Serilize(send), HttpMethod.Post, _route).ContinueWith(t =>
        //                {
        //                    try
        //                    {
        //                        var res = t.Result;
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        isCorrect = false;
        //                        results.Add(new HttpResult()
        //                        {
        //                            errors = new Error(ex)
        //                        });
        //                    }

        //                    var exceptions = results.Where(x => x?.errors?.exception != null).Select(x => x.errors.exception);
        //                    if (exceptions.Any())
        //                    {
        //                        isCorrect = false;
        //                        foreach (var exception in exceptions)
        //                        {
        //                            WorkflowActionError?.Invoke("Sending error.", exception);
        //                        }
        //                    }

        //                    skip += take;
        //                });
        //                continuation.Wait(5000);

        //            } while (DataCollection.Count > skip + take);
        //        }
        //        catch (Exception ex)
        //        {
        //            results.Add(new HttpResult()
        //            {
        //                errors = new Error(ex)
        //            });
        //            isCorrect = false;

        //            var exceptions = results.Where(x => x?.errors?.exception != null).Select(x => x.errors.exception);
        //            foreach (var exception in exceptions)
        //            {
        //                WorkflowActionError?.Invoke("Sending error.", exception);
        //            }
        //        }


        //        var cache =
        //            results.Where(
        //                x => x != null && x.errors.exception == null && x.errors.name.All(string.IsNullOrEmpty));

        //        if (cache.Any())
        //        {
        //            using (var odb = OdbFactory.Open(Path.Combine(WorkContext.BaseDirectory, dbFileName)))
        //            {
        //                var obj = odb.AsQueryable<CacheObject>()
        //                    .FirstOrDefault(x => x.ModuleGuid == MigrationModule.Guid);

        //                if (obj != null) odb.Delete(obj);

        //                odb.Store(new CacheObject()
        //                {
        //                    ModuleGuid = MigrationModule.Guid,
        //                    Objects = new List<HttpResult>(cache)
        //                });
        //            }
        //        }

        //        if (isCorrect) WorkflowActionSuccess?.Invoke("Data sending completed successfully");
        //        else WorkflowActionError?.Invoke("Sending data is completed with errors", null);

        //    }

        //}


    }
}
