﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Linq;
using Boss.Migration.Context;
using Boss.Migration.Helper;
using Boss.Migration.Http.Model;
using Boss.Migration.Models;
using Boss.Migration.Settings;
using Boss.MigrationCore.Common.Serilizer;
using Boss.MigrationCore.Context;
using Boss.MigrationCore.Logging;
using NDatabase;

namespace Boss.Migration.Workflow
{
    public class BaseMigrationWorkflow
    {
        public const string dbFileName = "Cache.db";

        protected string ConnectionString { get; set; }
        protected MigrationSettings MigrationSettings { get; set; }
        protected MigrationModule MigrationModule { get; set; }
        protected IWorkContext WorkContext { get; set; }
        protected ILog Logger { get; set; }

        public Action<string> WorkflowAction;
        public Action<string> WorkflowActionSuccess;
        public Action<string, Exception> WorkflowActionError;

        public List<ExpandoObject> DataCollection = new List<ExpandoObject>();

        public BaseMigrationWorkflow(string connectionString, MigrationSettings migrationSettings, MigrationModule migrationModule, IWorkContext workContext, ILog logger)
        {
            ConnectionString = connectionString;
            WorkContext = workContext;
            Logger = logger;
            MigrationSettings = migrationSettings;
            MigrationModule = migrationModule;

        }

        public virtual void Run()
        {
            WorkflowAction?.Invoke("Migration started. Getting data...");
            string query = MigrationModule.UseDateSelector && MigrationSettings.TicketSelectorIsEnabled
                ? $"{MigrationModule.Query} {MigrationModule.DateSelectorQuery.Replace("{D}", MigrationSettings.TicketSelectorDate.ToString())}"
                : MigrationModule.Query;


            bool isCorrect = GetData(query, ConnectionString, MigrationModule);
            if (isCorrect)
            {
                WorkflowAction?.Invoke($"Data received. Total records: {DataCollection.Count}. Started sending data to portal...");
                Save(DataCollection);
            }
        }

        public virtual bool GetData(string queryString, string connectionString, MigrationModule migrationModule)
        {
            bool isCorrect = true;
            DataCollection = new List<ExpandoObject>();

            try
            {
                using (var connection = new SqlConnection(connectionString))
                using (var command = new SqlCommand(queryString, connection))
                {
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                dynamic item = new ExpandoObject();
                                Dictionary<string, object> additionalFields = new Dictionary<string, object>();

                                foreach (var field in migrationModule.Fields.Where(x => x.IsEnabled))
                                {
                                    object resultValue = reader[field.SourceName];
                                    switch (field.TransformType)
                                    {
                                        case TransformType.Default:
                                        case TransformType.HierarchicalTransform:
                                            if (field.TransformType == TransformType.HierarchicalTransform)
                                            {
                                                var additionalField = CategoriesTransformConverter.ConvertAsAdditionalField(MigrationSettings.CurrentCategoriesTransformType, resultValue.ToString());
                                                if (!string.IsNullOrEmpty(additionalField.Key)) additionalFields.Add(additionalField.Key, additionalField.Value);
                                                resultValue = CategoriesTransformConverter.ConvertName(MigrationSettings.CurrentCategoriesTransformType, resultValue.ToString());

                                            }
                                            resultValue = field.ReplaceRecords.FirstOrDefault(x => x.SourceValue.ToLower() == resultValue.ToString().ToLower())?.TargetValue
                                                          ?? resultValue;
                                            break;

                                        case TransformType.DefaultReplaceWithCacheObject:
                                        case TransformType.HierarchicalTransformAndReplaceWithCacheObject:
                                            using (var odb = OdbFactory.Open(Path.Combine(WorkContext.BaseDirectory, dbFileName)))
                                            {
                                                if (field.TransformType == TransformType.HierarchicalTransformAndReplaceWithCacheObject)
                                                {
                                                    var additionalField = CategoriesTransformConverter.ConvertAsAdditionalField(MigrationSettings.CurrentCategoriesTransformType, resultValue.ToString());
                                                    if (!string.IsNullOrEmpty(additionalField.Key)) additionalFields.Add(additionalField.Key, additionalField.Value);
                                                    resultValue = CategoriesTransformConverter.ConvertName(MigrationSettings.CurrentCategoriesTransformType, resultValue.ToString());
                                                }
                                                var obj = odb.AsQueryable<CacheObject>().FirstOrDefault(x => field.ReplaceTransformObject != null && x.ModuleGuid == field.ReplaceTransformObject.ModuleGuid);
                                                if (obj != null)
                                                {
                                                    MigrationModule moduleSettings = null;
                                                    var js = new JsonFileSerilizer();
                                                    var path = Path.Combine(WorkContext.MigrationModulesSettingsDirectory, $"{field.ReplaceTransformObject.ModuleGuid}.json");
                                                    //module settings reading
                                                    try
                                                    {
                                                        if (File.Exists(path))
                                                        {
                                                            moduleSettings = js.Read<MigrationModule>(path);
                                                            var fieldName = moduleSettings.Fields.FirstOrDefault(x => x.Guid == field.ReplaceTransformObject.FieldGuid)?.TargetName;
                                                            if (fieldName != null)
                                                            {
                                                                resultValue =
                                                                    obj.Objects.FirstOrDefault(
                                                                        x => x.legacy_id.ToString() == resultValue.ToString())?.bossdesk_id;
                                                            }
                                                        }

                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        Logger.Error($"Method: [ReadModuleSettings]. Settings reading error", ex);
                                                    }
                                                }
                                            }
                                            break;

                                        case TransformType.HashCode:
                                            resultValue = field.ReplaceRecords.FirstOrDefault(x => x.SourceValue.ToLower() == resultValue.ToString().ToLower())?.TargetValue
                                                          ?? resultValue;
                                            resultValue = resultValue.GetHashCode();
                                            break;

                                        case TransformType.UuidTransform:
                                            resultValue = field.ReplaceRecords.FirstOrDefault(x => x.SourceValue.ToLower() == resultValue.ToString().ToLower())?.TargetValue
                                                          ?? resultValue;
                                            try
                                            {
                                                resultValue = Boss.MigrationCore.Common.Extensions.Uuid.NewNamespaceUuid(
                                                    Boss.MigrationCore.Common.Extensions.Uuid.DnsNamespace,
                                                    resultValue?.ToString());
                                            }
                                            catch (Exception ex)
                                            {
                                                Logger.Error("Get Uuid Error", ex);
                                            }
                                            break;

                                        case TransformType.SplitTransform:
                                            resultValue = field.ReplaceRecords.FirstOrDefault(x => x.SourceValue.ToLower() == resultValue.ToString().ToLower())?.TargetValue
                                                          ?? resultValue;

                                            if (!string.IsNullOrEmpty(field.SplitTransformSeparator))
                                            {
                                                if (resultValue != null && resultValue.ToString().Contains(field.SplitTransformSeparator))
                                                {
                                                    var arr =
                                                    resultValue?.ToString()
                                                        .Split(new string[] { field.SplitTransformSeparator },
                                                            StringSplitOptions.None);

                                                    if (arr.Length > field.SplitTransformIndex)
                                                    {
                                                        resultValue = arr[field.SplitTransformIndex];
                                                    }
                                                    else
                                                    {
                                                        resultValue = null;
                                                    }
                                                }
                                                else
                                                {
                                                    resultValue = field.SplitSetNullIfNotFoundSeparator ? null : resultValue;
                                                }
                                                
                                            }
                                            break;

                                        case TransformType.SplitAndUuidTransform:
                                            resultValue = field.ReplaceRecords.FirstOrDefault(x => x.SourceValue.ToLower() == resultValue.ToString().ToLower())?.TargetValue
                                                          ?? resultValue;

                                            if (!string.IsNullOrEmpty(field.SplitTransformSeparator))
                                            {
                                                if (resultValue != null && resultValue.ToString().Contains(field.SplitTransformSeparator))
                                                {
                                                    var arr =
                                                    resultValue?.ToString()
                                                        .Split(new string[] { field.SplitTransformSeparator },
                                                            StringSplitOptions.None);

                                                    if (arr.Length > field.SplitTransformIndex)
                                                    {
                                                        resultValue = arr[field.SplitTransformIndex];
                                                    }
                                                    else
                                                    {
                                                        resultValue = null;
                                                    }
                                                }
                                                else
                                                {
                                                    resultValue = field.SplitSetNullIfNotFoundSeparator ? null : resultValue;
                                                }

                                            }

                                            try
                                            {
                                                resultValue = Boss.MigrationCore.Common.Extensions.Uuid.NewNamespaceUuid(
                                                    Boss.MigrationCore.Common.Extensions.Uuid.DnsNamespace,
                                                    resultValue?.ToString());
                                            }
                                            catch (Exception ex)
                                            {
                                                Logger.Error("Get Uuid Error", ex);
                                            }

                                            break;


                                    }
                                    //object sourceValue = reader[field.SourceName];
                                    //sourceValue = field.TransformType == TransformType.HierarchicalTransform || field.TransformType == TransformType.HierarchicalTransformAndReplaceWithCacheObject ? CategoriesTransformConverter.Convert(MigrationSettings.CurrentCategoriesTransformType, reader[field.SourceName].ToString()) : reader[field.SourceName];
                                    //var findReplace = field.ReplaceRecords.FirstOrDefault(x => x.SourceValue.ToLower() == reader[field.SourceName].ToString().ToLower());

                                    ((IDictionary<string, object>)item)[field.TargetName] = resultValue;//findReplace == null ? sourceValue : findReplace.TargetValue;
                                }

                                foreach (var additionalField in additionalFields)
                                {
                                    ((IDictionary<string, object>)item)[additionalField.Key] = additionalField.Value;
                                }

                                DataCollection.Add(item);
                            }
                        }
                }
            }
            catch (System.Exception ex)
            {
                isCorrect = false;
                WorkflowActionError?.Invoke("Failed to get the data.", ex);
            }

            return isCorrect;
        }

        public virtual void Save(IEnumerable<ExpandoObject> items)
        {
            try
            {
                JsonFileSerilizer js = new JsonFileSerilizer();
                var path = Path.Combine(WorkContext.DataDirectory, $"{MigrationModule.Caption}_{MigrationModule.Guid}.json");
                js.Write(path, items);
                WorkflowActionSuccess?.Invoke($"Saving completed.");
            }
            catch (System.Exception ex)
            {
                WorkflowActionError?.Invoke("Could not save data.", ex);
            }
        }
    }
}
