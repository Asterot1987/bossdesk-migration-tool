﻿using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using Boss.Migration.Context;
using Boss.Migration.Http.Model;
using Boss.Migration.Http.Request;
using Boss.Migration.Models;
using Boss.Migration.Settings;
using Boss.MigrationCore.Common.Serilizer;
using Boss.MigrationCore.Context;
using Boss.MigrationCore.Logging;
using NDatabase;
using Newtonsoft.Json;
using RestSharp;

namespace Boss.Migration.Workflow
{
    public class DefaultMigrationWorkflow : BaseMigrationWorkflow
    {
        public DefaultRequest Request { get; set; }
        private ISerilizer _serilizer;

        public DefaultMigrationWorkflow(string connectionString, string route, MigrationSettings migrationSettings, MigrationModule migrationModule, IWorkContext workContext, ILog logger, ISerilizer serilizer) : base(connectionString, migrationSettings, migrationModule, workContext, logger)
        {
            _serilizer = serilizer;

            Request = new DefaultRequest(migrationSettings, Logger, _serilizer)
            {
                //todo add route reading
                Route = route
            };
        }

        public override void Save(IEnumerable<ExpandoObject> items)
        {
            //for test
            try
            {
                JsonFileSerilizer js = new JsonFileSerilizer();
                var path = Path.Combine(WorkContext.DataDirectory, $"{MigrationModule.Caption}_{MigrationModule.Guid}.json");
                js.Write(path, items);
            }
            catch (System.Exception ex)
            {
                Logger.Error("Could not save data.", ex);
            }

            bool isCorrect = true;
            List<HttpResult> results = new List<HttpResult>();

            if (DataCollection != null && DataCollection.Count > 0)
            {
                int take = 500;
                int skip = 0;

                Logger.Info($"Start sending of {DataCollection.Count} records");
                Logger.Info($"Request behavior: [{take}] records per request");

                do
                {
                    if (skip > 0) base.Logger.Info($"Sent [{skip}] from [{DataCollection.Count} records");
                    Dictionary<string, IEnumerable<ExpandoObject>> send = new Dictionary
                        <string, IEnumerable<ExpandoObject>>()
                    {
                        {Request.Route, DataCollection.Skip(skip).Take(take).Select(x => x).ToList()}
                    };

                    var data = Request.Exeсute(Method.POST, _serilizer.Serilize(send)) as List<HttpResult>;
                    results.AddRange(data);

                    var exceptions = results.Where(x => x?.errors?.exception != null).Select(x => x.errors.exception);

                    if (exceptions.Any())
                    {
                        isCorrect = false;
                        foreach (var exception in exceptions)
                        {
                            WorkflowActionError?.Invoke("Sending error.", exception);
                        }
                    }
                    skip += take;
                } while (DataCollection.Count > skip);

                var cache =
                    results.Where(
                        x => x != null && x.errors.exception == null && x.errors.name.All(string.IsNullOrEmpty));

                if (cache.Any())
                {
                    using (var odb = OdbFactory.Open(Path.Combine(WorkContext.BaseDirectory, dbFileName)))
                    {
                        var obj = odb.AsQueryable<CacheObject>()
                            .FirstOrDefault(x => x.ModuleGuid == MigrationModule.Guid);

                        if (obj != null) odb.Delete(obj);

                        odb.Store(new CacheObject()
                        {
                            ModuleGuid = MigrationModule.Guid,
                            Objects = new List<HttpResult>(cache)
                        });
                    }
                }

                if (isCorrect) WorkflowActionSuccess?.Invoke("Data sending completed successfully");
                else WorkflowActionError?.Invoke("Sending data is completed with errors", null);

            }

        }


    }
}
