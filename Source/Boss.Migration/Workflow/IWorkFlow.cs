﻿
using System;

namespace Boss.Migration.Workflow
{
    public interface IWorkFlow
    {
        /// <summary>
        /// just for identification
        /// </summary>
        string Name { get; }

        Action<IWorkFlow> Completed { get; set; }


        void StartWorkflow(object targets);

    }
}
