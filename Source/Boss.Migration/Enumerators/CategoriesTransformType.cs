﻿namespace Boss.Migration.Enumerators
{
    public enum CategoriesTransformType
    {
        AllDash, // " - "
        AllSpace,  // " "
        AllUnderline, // "_"
        GroupOnly,
        GroupAndCategoryAsTag,
        GroupAndCategoryAsCustomField
    }
}
