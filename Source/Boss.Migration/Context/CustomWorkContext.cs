﻿using System;
using System.IO;
using Boss.MigrationCore.AccessControl;
using Boss.MigrationCore.Context;
using Boss.MigrationCore.Enumerators;

namespace Boss.Migration.Context
{
    public class CustomWorkContext : IWorkContext
    {
        public string ProjectName;
        public string CommonFolderBaseName;// = @"BOSSDesk Migration Tool\";

        public CustomWorkContext(string projectName, MigrationTarget migrationTarget)
        {
            MigrationTarget = migrationTarget;
            ProjectName = projectName;
            CommonFolderBaseName = Constants.DefaultMigrationConstantConfigs[MigrationTarget].DefaultFolder;

            

            using (var sac = new SetAccessControl())
            {
                if (!Directory.Exists(BaseDirectory))
                {
                    Directory.CreateDirectory(BaseDirectory);
                    sac.SetDirectoryAccessControl(BaseDirectory);
                }

                if (!Directory.Exists(LogDirectory))
                {
                    Directory.CreateDirectory(LogDirectory);
                    sac.SetDirectoryAccessControl(LogDirectory);
                }

                if (!Directory.Exists(SettingsDirectory))
                {
                    Directory.CreateDirectory(SettingsDirectory);
                    sac.SetDirectoryAccessControl(SettingsDirectory);
                }

                if (!Directory.Exists(MigrationModulesSettingsDirectory))
                {
                    Directory.CreateDirectory(MigrationModulesSettingsDirectory);
                    sac.SetDirectoryAccessControl(MigrationModulesSettingsDirectory);
                }

                if (!Directory.Exists(DataDirectory))
                {
                    Directory.CreateDirectory(DataDirectory);
                    sac.SetDirectoryAccessControl(DataDirectory);
                }
            }

        }

        public MigrationTarget MigrationTarget { get; set; }

        public virtual string CommonFolderName => $@"{CommonFolderBaseName}{ProjectName}\";

        public virtual string BaseDirectory
            => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), CommonFolderName);

        public virtual string LogDirectory
            => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), CommonFolderBaseName + @"Logs\");

        public virtual string SettingsDirectory
            => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), CommonFolderName + @"Settings\");

        public virtual string MigrationModulesSettingsDirectory
            => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), CommonFolderName + @"Settings\MigrationModules\");

        public virtual string DataDirectory
            => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), CommonFolderName + @"Data\");
    }
}
