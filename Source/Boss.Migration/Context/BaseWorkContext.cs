﻿using System;
using System.IO;
using Boss.MigrationCore.AccessControl;
using Boss.MigrationCore.Context;
using Boss.MigrationCore.Enumerators;

namespace Boss.Migration.Context
{
    public class BaseWorkContext : IWorkContext
    {
        public string CommonFolderBaseName;// = @"BOSSDesk Migration Tool\";

        public BaseWorkContext(MigrationTarget migrationTarget)
        {
            MigrationTarget = migrationTarget;
            CommonFolderBaseName = Constants.DefaultMigrationConstantConfigs[MigrationTarget].DefaultFolder;

            using (var sac = new SetAccessControl())
            {
                if (!Directory.Exists(BaseDirectory))
                {
                    Directory.CreateDirectory(BaseDirectory);
                    sac.SetDirectoryAccessControl(BaseDirectory);
                }
            }

        }

        public MigrationTarget MigrationTarget { get; set; }

        public virtual string BaseDirectory
            => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), CommonFolderBaseName);

        public virtual string LogDirectory
            => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), CommonFolderBaseName + @"Logs\");

        public virtual string SettingsDirectory
            => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), CommonFolderBaseName);

        public virtual string MigrationModulesSettingsDirectory
            => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), CommonFolderBaseName);

        public virtual string DataDirectory
            => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), CommonFolderBaseName);
    }
}
