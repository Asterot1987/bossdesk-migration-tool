﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Boss.BaseControls.Models;
using Boss.Migration.Enumerators;
using Boss.Migration.Models;
using Boss.MigrationCore.Enumerators;

namespace Boss.Migration
{

    public class MigrationConstantConfig
    {
        public string AppName;
        public string ShortAppName;
        public string DefaultFolder;
        public string LoggerConfigName;
        public RangeEnabledObservableCollection<KeyValue> DefaultProject;
    }

    public class Constants
    {
        public static string LegacyId = "legacy_id";

        public static string TicketCategoryAsTag = "tag";
        public static string TicketCategoryAsCustomField = "category";

        public static MigrationTarget CurrentMigrationTarget { get; set; }

        public static Dictionary<MigrationTarget, MigrationConstantConfig> DefaultMigrationConstantConfigs = new Dictionary
            <MigrationTarget, MigrationConstantConfig>()
        {
            {
                MigrationTarget.BossDesk,
                new MigrationConstantConfig()
                {
                    AppName = @"BOSSDesk Migration Tool",
                    ShortAppName = "BOSSDesk Migration Tool",
                    DefaultFolder = @"BOSSDesk Migration Tool\",
                    LoggerConfigName = "BDLog",
                    DefaultProject = new RangeEnabledObservableCollection<KeyValue>()
                    {
                        new KeyValue() {Key = "{06911403-0B2B-4A76-B73B-7C345B14B94D}", Value = "Support Central"}
                    }
                }
            },
            {
                MigrationTarget.BossSupportCentral,
                new MigrationConstantConfig()
                {
                    AppName = @"BSC Migration Tool",
                    ShortAppName = @"BSC Migration Tool",
                    DefaultFolder = @"BSC Migration Tool\",
                    LoggerConfigName = "BSCLog",
                    DefaultProject = new RangeEnabledObservableCollection<KeyValue>()
                    {
                        new KeyValue() {Key = "{1B648A67-F529-4EB3-94E7-2437792962A0}", Value = "Support Central 3.99"}
                    }
                }
            },
        }; 

        //public static Dictionary<MigrationTarget, string> DefaultMigrationFolders = new Dictionary<MigrationTarget, string>()
        //{
        //    { MigrationTarget.BossDesk, "BOSSDesk Migration Tool"},
        //    { MigrationTarget.BossSupportCentral, "BOSS Support Central Migration Tool"},
        //};

        //public static Dictionary<MigrationTarget, KeyValue> DefaultProjects = new Dictionary<MigrationTarget, KeyValue>()
        //{
        //    { MigrationTarget.BossDesk, new KeyValue() { Key = "{06911403-0B2B-4A76-B73B-7C345B14B94D}", Value = "Support Central" }},
        //    { MigrationTarget.BossSupportCentral, new KeyValue() { Key = "{1B648A67-F529-4EB3-94E7-2437792962A0}", Value = "Support Central 3.99" }},
        //};

        public static Dictionary<MigrationTarget, Dictionary<string, List<CheckGuidWithRouteModel>>>
            DefaultsCollectionsForProjects = new Dictionary<MigrationTarget, Dictionary<string, List<CheckGuidWithRouteModel>>>()
            {
                {
                    MigrationTarget.BossDesk,
                    new Dictionary<string, List<CheckGuidWithRouteModel>>()
                    {
                        {
                            "Support Central",
                            new List<CheckGuidWithRouteModel>()
                            {
                                new CheckGuidWithRouteModel("{EC16A58E-98DB-4971-BA4B-4CD730A9EF6A}", "Severity", "helpdesk_priorities", true),
                                new CheckGuidWithRouteModel("{8B904206-C42A-466A-A294-C023C98D835D}", "Extended Statuses", "helpdesk_extended_statuses", true),
                                new CheckGuidWithRouteModel("{f4074d8c-4e9e-46c2-9a98-16e49654e38c}", "Groups & Categories", "helpdesk_categories", true),
                                new CheckGuidWithRouteModel("{E43C9B57-46F8-4FCE-9D31-8ED509CD5523}", "Custom Fields", "", false),
                                new CheckGuidWithRouteModel("{9A4287D5-47D3-4155-9C54-A18DD4AAFA8A}", "Timesheet Tasks", "task_types", true),
                                new CheckGuidWithRouteModel("{a5058db6-cc2e-4ff9-8500-555174cdd518}", "Helpdesk Teams", "teams", true),
                                new CheckGuidWithRouteModel("{EA2BFF7A-77B1-42EE-A69D-2F97FC00B683}", "Contacts", "", false),
                                new CheckGuidWithRouteModel("{83A496B0-AA3C-44F3-B9CA-2D3F992ACCC4}", "E-mail Settings", "", false),
                                new CheckGuidWithRouteModel("{D9D26CCB-E5FF-4F5F-9239-DDB1EE743B31}", "Tickets", "", false),
                                new CheckGuidWithRouteModel("{A266E116-3CFE-46CB-8ECE-B300B154EA03}", "Additional & Custom Fields", "", false),
                                new CheckGuidWithRouteModel("{F1D6E97C-987A-4654-8183-85B3CBD474B9}", "Timesheet", "", false),
                                new CheckGuidWithRouteModel("{40787286-C43B-4DFD-8BB1-D982CA10E1FD}", "Attachment", "", false),
                            }
                        }
                    }
                },

                {
                    MigrationTarget.BossSupportCentral,
                    new Dictionary<string, List<CheckGuidWithRouteModel>>()
                    {
                        {
                            "Support Central 3.99",
                            new List<CheckGuidWithRouteModel>()
                            {
                                new CheckGuidWithRouteModel("{00B95226-1989-4F96-BB0C-A024861EB3CA}", "Users", "ImportUsers", true),
                                new CheckGuidWithRouteModel("{b961170a-996d-4650-9693-037972e12fc9}", "Technicians", "ImportUsers", true),
                                new CheckGuidWithRouteModel("{0d3320fb-224f-4081-91dc-cbbb5b1d0089}", "Managers", "ImportManagers", true),
                            }
                        }
                    }
                },

            };

    }
}
