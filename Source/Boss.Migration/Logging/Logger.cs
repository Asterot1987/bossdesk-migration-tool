﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Boss.MigrationCore.AccessControl;
using Boss.MigrationCore.Enumerators;
using Boss.MigrationCore.Logging;
using NLog;
using NLog.Config;

namespace Boss.Migration.Logging
{
    public class Logger : ILog
    {

        private readonly NLog.Logger _logger;
        private string FileName;

        public Logger(MigrationTarget migrationTarget)
        {


            //try
            //{
            //    string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            //    var assembly = Assembly.GetExecutingAssembly();
            //    var resourceName = assemblyFolder +
            //                       $@"\Logging\Config\{
            //                           Constants.DefaultMigrationConstantConfigs[Constants.CurrentMigrationTarget]
            //                               .LoggerName}.config";

            //    using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            //        if (stream != null)
            //            using (StreamReader reader = new StreamReader(stream))
            //            {
            //                var result = reader.ReadToEnd();
            //                NLog.LogManager.Configuration = new XmlLoggingConfiguration();
            //            }
            //}
            //catch (Exception ex)
            //{
            //}

            string assemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            NLog.LogManager.Configuration = new NLog.Config.XmlLoggingConfiguration(assemblyFolder + $@"\Logging\Config\{Constants.DefaultMigrationConstantConfigs[Constants.CurrentMigrationTarget].LoggerConfigName}.config", true);
            _logger = NLog.LogManager.GetLogger(Constants.DefaultMigrationConstantConfigs[migrationTarget].ShortAppName);

            var currentLog = (NLog.Targets.FileTarget)NLog.LogManager.Configuration.FindTargetByName("file");
            var logEventInfo = new LogEventInfo { TimeStamp = DateTime.Now };
            FileName = currentLog.FileName.Render(logEventInfo);
            var dir = Path.GetDirectoryName(FileName);
            using (var sac = new SetAccessControl())
            {

                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                    sac.SetDirectoryAccessControl(dir);
                }

                if (!File.Exists(FileName))
                    File.CreateText(FileName);
                sac.SetFileAccessControl(FileName);
            }

        }

        public void Warning(string message)
        {
            _logger.Warn(message);
        }

        public void Info(string message)
        {
            _logger.Info(message);
        }

        public void Error(string message)
        {
            _logger.Error(message);
        }

        public void Debug(string message)
        {
            _logger.Debug(message);
        }

        public void Error(string message, Exception exception)
        {

            _logger.Error(exception, message);

        }


        private LogModel ParseLine(string dataToParse)
        {
            var model = new LogModel();
            try
            {
                var splitedKeyPairs = dataToParse.Split(
                    new[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                Array.ForEach(splitedKeyPairs, keyPair =>
                {
                    if (keyPair.Contains("Message:"))
                    {
                        model.Message = keyPair.Trim().Replace("Message:", "").Trim();
                    }
                    if (keyPair.Contains("Date:"))
                    {
                        string dateSting = keyPair.Trim().Replace("Date:", "").Trim();
                        model.Date = DateTime.Parse(dateSting);
                    }
                    if (keyPair.Contains("Module:"))
                    {
                        model.ModuleName = keyPair.Trim().Replace("Module:", "").Trim();
                    }
                });
            }
            catch (Exception)
            {
                //
            }
            return model;
        }

        /// <summary>
        /// Read data from log
        /// </summary>
        /// <returns></returns>
        public List<LogModel> GetLogs()
        {
            string flushData;
            var logModels = new List<LogModel>();
            using (var stream = File.OpenText(FileName))
            {
                while (!stream.EndOfStream)
                {
                    flushData = stream.ReadLine();
                    if (!string.IsNullOrEmpty(flushData))
                    {
                        var logModel = ParseLine(flushData);
                        logModels.Add(logModel);
                    }
                }
            }
            return logModels;

        }
    }
}