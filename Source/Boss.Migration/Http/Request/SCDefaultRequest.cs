﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Boss.Migration.Http.Connection;
using Boss.Migration.Http.Model;
using Boss.Migration.Settings;
using Boss.Migration.Workflow;
using Boss.MigrationCore.Common;
using Boss.MigrationCore.Common.Serilizer;
using Boss.MigrationCore.Logging;
using RestSharp;

namespace Boss.Migration.Http.Request
{

    public class SCRestConnection : RestConnection
    {
        public SCRestConnection(ISettings settings) : base(settings)
        {
            Settings = settings as MigrationSettings;
            Parameters = new Dictionary<string, object>
            {
                {"Authorization", $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes($"{Settings.ApiKey}:"))}"},//$"Token token={Settings.ApiKey}"},
                {"Content-Type", $"application/json"}
            };
        }

        public override RestClient CreateConnection()
        {
            if (Client == null)
            {
                string url = $"{Settings.BossDeskUrl}/api/v1.0";
                Client = new RestClient(url);
            }

            return Client;
        }
    }

    public class SCRequest: BaseRequest
    {
        private ISerilizer _serilizer;

        public SCRequest(ISettings settings, ILog logging, ISerilizer serilizer) : base(settings, logging)
        {
            _serilizer = serilizer;
            Connection = new SCRestConnection(settings);
        }



        protected override object MapResult(IRestResponse response)
        {
            if (!string.IsNullOrEmpty(response.Content))
            {
                try
                {
                    Results = new List<HttpResult>();
                    var res = _serilizer.Deserilize<Dictionary<string, List<HttpResult>>>(response.Content);
                    Results = res.FirstOrDefault().Value;
                }
                catch (Exception ex)
                {
                    Logger.Error("HttpRequest [MapResult] error. Content: " + response.Content, ex);
                    Results.Add(new HttpResult()
                    {
                        bossdesk_id = null,
                        legacy_id = null,
                        errors = new Error(new Exception(response.Content))
                    });
                }

            }
            else if (response.StatusCode != HttpStatusCode.OK)
            {
                Logger.Error("HttpRequest [MapResult] error. StatusCode: " + response.StatusCode, response.ErrorException);
                Results.Add(new HttpResult()
                {
                    bossdesk_id = null,
                    legacy_id = null,
                    errors = new Error(new Exception($"HttpRequest error. Status Code: {response.StatusCode}. {response.ErrorException?.Message ?? ""}" ))
                });

            }

            return Results;
        }


    }
}
