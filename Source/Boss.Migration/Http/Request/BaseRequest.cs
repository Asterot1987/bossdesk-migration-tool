﻿using System;
using System.Collections.Generic;
using System.Security.Authentication;
using Boss.Migration.Http.Connection;
using Boss.Migration.Http.Model;
using Boss.Migration.Settings;
using Boss.MigrationCore.Common;
using Boss.MigrationCore.Logging;
using RestSharp;

namespace Boss.Migration.Http.Request
{
    public abstract class BaseRequest : IRequestQuery
    {

        #region Privates
        private const string BodyParameter = "application/json; charset=utf-8";
        protected IConnection<RestClient> Connection;
        private string _route;

        #endregion

        #region Protected properties
        protected ILog Logger;
        public MigrationSettings Settings;
        protected RestRequest Request;
        protected abstract object MapResult(IRestResponse response);
        protected Action StartingRequest { get; set; }
        #endregion

        #region Implementation of IRequestQuery
        public string Route { get; set; } = "";
        public List<HttpResult> Results { get; set; }


        protected BaseRequest(ISettings settings, ILog logging)
        {
            Settings = settings as MigrationSettings;

            if (Settings != null)
            {
                Connection = new RestConnection(Settings);
                Logger = logging;
                Results = new List<HttpResult>();
            }
        }

        protected virtual void CreateRequest(Method method = Method.GET)
        {

            Request = new RestRequest(Route, method);
            InitParams((Connection as RestConnection).Parameters);
            Results.Clear();
        }

        protected void InitParams(Dictionary<string, object> paramRequest = null)
        {
            foreach (var parameter in paramRequest)
            {
                Request.AddHeader(parameter.Key, parameter.Value.ToString());
            }
        }

        public virtual object Exeсute(Method method = Method.POST, string body = null)
        {
            object result = new object();
            StartingRequest?.Invoke();
            CreateRequest(method);
            if (!string.IsNullOrEmpty(body))
            {
                Request.AddParameter(BodyParameter, body, ParameterType.RequestBody);
            }
            try
            {
                var response = Connection.CreateConnection().Execute(Request);
                result = MapResult(response);
                Logger.Info($"Request [{Route}] with result {response.Content}");
            }
            catch (Exception ex)
            {
                Logger.Error($"Error sending data to [{Connection.CreateConnection().BaseUrl}] with route [{Route}]", ex);
                result = new HttpResult()
                {
                    bossdesk_id = null,
                    legacy_id = null,
                    errors = new Error(ex)
                };
            }
            Results.Add(result as HttpResult);

            return result;
        }

        #endregion
    }
}
