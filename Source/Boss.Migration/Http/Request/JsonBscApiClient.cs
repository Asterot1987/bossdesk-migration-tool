﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SupportCentral.WebApi.Client.Base;
using SupportCentral.WebApi.Client.Settings;
using SupportCentral.WebApi.Models.Base;

namespace Boss.Migration.Http.Request
{
    public class JsonBscApiClient<TExternalId, TInternalId> : BscApiClient
    {
        public JsonBscApiClient(IBscApiSettings apiSettings) : base(apiSettings)
        {
        }

        protected override HttpContent SerializeBody(object body)
        {
            return new StringContent(body.ToString(), Encoding.UTF8, "application/json");
        }

        //public async Task<BaseResponse<List<IdPair<TExternalId, TInternalId>>>> ImportJsonList<TExternalId, TInternalId>(string json, HttpMethod method, string action)
        //{
        //    //var res = await ExecuteRequest<BaseResponse>(action, method, json);
        //    var res = await ExecuteRequest<BaseResponse<List<IdPair<TExternalId, TInternalId>>>>(action, method, json);
        //    return res;
        //}

        public BaseResponse<List<IdPair<TExternalId, TInternalId>>> ImportJsonList(string json, HttpMethod method, string action)
        {
            var res = ExecuteRequest<BaseResponse<List<IdPair<TExternalId, TInternalId>>>>(action, method, json).Result;
            return res;
        }

        public async Task<BaseResponse> ImportJsonListBase(string json, HttpMethod method, string action)
        {
            var res = await ExecuteRequest<BaseResponse>(action, method, json);
            return res;
        }


    }
}
