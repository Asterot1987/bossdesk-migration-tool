﻿using System.Collections.Generic;
using Boss.Migration.Http.Model;
using RestSharp;

namespace Boss.Migration.Http.Request
{
    public interface IRequestQuery
    {
        string Route { get; set; }
        object Exeсute(Method method = Method.POST, string body = null);

        List<HttpResult> Results { get; set; }
    }
}
