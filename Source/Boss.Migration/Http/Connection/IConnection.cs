﻿using System;

namespace Boss.Migration.Http.Connection
{
    public interface IConnection<T> : IDisposable
    {
        T CreateConnection();
    }
}
