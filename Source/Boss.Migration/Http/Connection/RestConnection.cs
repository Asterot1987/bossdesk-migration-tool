﻿using System.Collections.Generic;
using Boss.Migration.Settings;
using Boss.MigrationCore.Common;
using RestSharp;

namespace Boss.Migration.Http.Connection
{
    public class RestConnection : IConnection<RestClient>
    {
        protected RestClient Client;
        protected MigrationSettings Settings;
        public Dictionary<string, object> Parameters;
#if DEBUG
        protected string Scheme = "https";
#else
        protected string Scheme = "https";
#endif
        protected string MainDomain = "bossdesk.io";
        public string ApiUrlPArt { get; set; } = "api/mig/sc";
        public string ApiVerion { get; set; } = "v1";


        public RestConnection(ISettings settings)
        {
            Settings = settings as MigrationSettings;
            Parameters = new Dictionary<string, object>
            {
                {"Authorization", $"Token token={Settings.ApiKey}"},
                {"Content-Type", $"application/json"}
            };
        }


        public virtual RestClient CreateConnection()
        {
            if (Client == null)
            {
                string url = $"{Scheme}://{Settings.BossDeskUrl}.{MainDomain}/{ApiUrlPArt}/{ApiVerion}";
                Client = new RestClient(url);
            }

            return Client;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

    }
}
