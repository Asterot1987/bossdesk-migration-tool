﻿using System;
using System.Collections.Generic;
using System.Dynamic;

namespace Boss.Migration.Http.Model
{
    public class Error
    {
        public List<string> name { get; set; }

        public Exception exception { get; set; }

        public Error()
        {
            name = new List<string>();
        }

        public Error(string error)
        {
            name = new List<string>() { { error } };
        }

        public Error(Exception ex)
        {
            name = new List<string>();
            exception = ex;
        }
        

    }
}
