﻿namespace Boss.Migration.Http.Model
{
    public class HttpResult
    {
        public object legacy_id { get; set; }

        public object bossdesk_id { get; set; }

        public Error errors { get; set; }

        public HttpResult()
        {
            errors = new Error();
        }

    }

}
