﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Boss.BaseControls.Controls;
using Boss.BaseControls.Models;
using Boss.GuiCore.Enumerators;
using Boss.GuiCore.Models;
using Boss.GuiCore.ViewModels.BaseImplementations;
using Boss.Migration;
using Boss.Migration.Helper;
using Boss.Migration.Logging;
using Boss.Migration.Models;
using Boss.Migration.Settings;
using Boss.Migration.Workflow;
using Boss.MigrationCore.Common.Serilizer;
using Boss.MigrationCore.Context;
using Boss.MigrationCore.Logging;
using MigrationSettingsWindow = SupportCentral.MigrationTool.Views.MigrationSettingsWindow;

namespace SupportCentral.MigrationTool.ViewModels
{
    public class MigrationProgressViewModel : BaseWindowViewModel<string>
    {
        private AdvancedWindow _win;
        private SqlConnectionSettings _sqlConnectionSettings;
        private MigrationSettings _migrationSettings;
        private RangeEnabledObservableCollection<MigrationStep> _steps = new RangeEnabledObservableCollection<MigrationStep>();
        private RangeEnabledObservableCollection<EventLogMessage> _logs = new RangeEnabledObservableCollection<EventLogMessage>();
        private bool _showOnlyErrors;
        private bool _migrationIsCompleted;
        private double _stepLineHeight;
        private double _windowsMinHeight = 520;
        private bool _scrollEventLog = true;

        public string Header { get; set; }

        public SqlConnectionSettings SqlConnectionSettings
        {
            get { return _sqlConnectionSettings; }
            set
            {
                _sqlConnectionSettings = value;
                OnPropertyChanged();
            }
        }

        public MigrationSettings MigrationSettings
        {
            get { return _migrationSettings; }
            set
            {
                _migrationSettings = value;
                OnPropertyChanged();
            }
        }

        public MigrationProjectsSettings ProjectsSettings { get; set; }

        public RangeEnabledObservableCollection<MigrationStep> Steps
        {
            get { return _steps; }
            set
            {
                _steps = value;
                OnPropertyChanged();
            }
        }

        public RangeEnabledObservableCollection<EventLogMessage> Logs
        {
            get { return _logs; }
            set
            {
                _logs = value;
                OnPropertyChanged();
            }
        }

        public bool MigrationIsCompleted
        {
            get { return _migrationIsCompleted; }
            set
            {
                _migrationIsCompleted = value;
                OnPropertyChanged();
            }
        }

        public bool ShowOnlyErrors
        {
            get { return _showOnlyErrors; }
            set
            {
                ScrollEventLog = false;
                _showOnlyErrors = value;
                OnPropertyChanged();
                ScrollEventLog = true;
            }
        }

        public double StepLineHeight
        {
            get { return _stepLineHeight; }
            set
            {
                _stepLineHeight = value; 
                OnPropertyChanged();
            }
        }

        public double WindowsMinHeight
        {
            get { return _windowsMinHeight; }
            set
            {
                _windowsMinHeight = value;
                OnPropertyChanged();
            }
        }

        public bool ScrollEventLog
        {
            get { return _scrollEventLog; }
            set
            {
                _scrollEventLog = value; 
                OnPropertyChanged();
            }
        }

        public MigrationProgressViewModel(MigrationProjectsSettings projectsSettings, ILog logger, IWorkContext workContext, SqlConnectionSettings sqlConnectionSettings, MigrationSettings migrationSettings, AdvancedWindow win) : base(logger, workContext)
        {
            _win = win;
            WorkContext = workContext;
            ProjectsSettings = projectsSettings;
            SqlConnectionSettings = sqlConnectionSettings;
            MigrationSettings = migrationSettings;
            
            InitData();
            Run();
        }

        public override bool InitData()
        {
            bool res = true;
            IsBusy = true;
            BusyContent = Properties.Resources.BusyContent_DataLoading;

            try
            {
                Logger.Info($"Object: Migration Process. Method: [InitData]. SQLSettings reading started");
                SqlConnectionSettings.Read(Path.Combine(WorkContext.SettingsDirectory, $"{SqlConnectionSettings.WorkflowId}.json"));
                Logger.Info($"Object: Migration Process. Method: [InitData]. SQLSettings reading completed");
            }
            catch (Exception ex)
            {
                Logger.Error($"Object: Migration Process. Method: [InitData]. SQLSettings reading error", ex);
                res = false;
            }


            try
            {
                Logger.Info($"Object: Migration Process. Method: [InitData]. Migration Settings reading started");
                MigrationSettings.Read(Path.Combine(WorkContext.SettingsDirectory, $"{MigrationSettings.WorkflowId}.json"));
                Logger.Info($"Object: Migration Process. Method: [InitData]. Migration Settings reading completed");

                //check modules
                foreach (var module in MigrationSettings.MigrationModules)
                {
                    var path = Path.Combine(WorkContext.MigrationModulesSettingsDirectory, $"{module.Guid}.json");

                    //write default
                    if (!File.Exists(path))
                    {
                        var moduleFolder = ProjectsSettings.LastProject.Value.ToString().Replace(" ", "_");
                        moduleFolder = moduleFolder.Replace(".", "._");
                        var defModulesOperations = new DefaultModulesOperations(Logger);
                        var moduleData = defModulesOperations.ReadModule(
                                $"Boss.Migration.ModulesResources.{moduleFolder}.",
                                $"{module.Guid}.json");

                        var result = defModulesOperations.WriteModule(
                            WorkContext.MigrationModulesSettingsDirectory,
                            $"{module.Guid}.json",
                            moduleData
                            );
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"Object: Migration Process. Method: [InitData]. Migration Settings reading error", ex);
                res = false;
            }


            int index = 0;
            Steps = new RangeEnabledObservableCollection<MigrationStep>(MigrationSettings.MigrationModules.Where(x => x.IsEnabled).Select(x => new MigrationStep()
            {
                Index = ++index,
                Caption = x.Caption,
                Guid = x.Guid,
                Route = x.Route
            }));
            StepLineHeight = Steps.Count > 0 ? (Steps.Count - 1) * 40 : 0;

            var tempHeight = Steps.Count*40 + 120;
            if (WindowsMinHeight < tempHeight) WindowsMinHeight = tempHeight;

            IsBusy = false;
            return res;
        }

        public override void InitCommands()
        {
            base.InitCommands();

            GoToBackCommand = new Command(o =>
            {
                try
                {
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        var main = new MigrationSettingsWindow();
                        main.DataContext = new MigrationSettingsViewModel(ProjectsSettings, new Logger(Constants.CurrentMigrationTarget), WorkContext, new MigrationSettings(new JsonFileSerilizer()), main);
                        Application.Current.MainWindow = main;
                        _win.Close();
                        main.Show();
                    }));
                }
                catch (Exception ex)
                {
                    Logger.Error($"Object: SQLConnection. Method: [GoToNext]. Go to next step error", ex);
                }
            });
        }

        private void Run()
        {
            var bg = new BackgroundWorker();
            bg.DoWork += (s, a) =>
            {
                foreach (var step in Steps)
                {
                    step.State = MigrationStepState.InProgress;

                    var moduleSettings = new MigrationModule();
                    var js = new JsonFileSerilizer();
                    var path = Path.Combine(WorkContext.MigrationModulesSettingsDirectory, $"{step.Guid}.json");

                    //settings reading
                    try
                    {
                        if (File.Exists(path))
                        {
                            moduleSettings = js.Read<MigrationModule>(path);
                        }
                        else
                        {
                            Logger.Error($"Object: {step.Caption}. Method: [InitModuleSettings]. File not found", null);
                            Application.Current.Dispatcher.Invoke((Action)(() =>
                            {
                                Logs.Add(new EventLogMessage()
                                {
                                    Type = EventLogMessageType.Error,
                                    Date = DateTime.Now,
                                    Caption = step.Caption,
                                    Message = $"Сonfiguration file could not be found"
                                });
                            }));
                            step.State = MigrationStepState.Error;
                            continue;
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Error($"Object: {step.Caption}. Method: [InitModuleSettings]. Settings reading error", ex);
                        Application.Current.Dispatcher.Invoke((Action)(() =>
                        {
                            Logs.Add(new EventLogMessage()
                            {
                                Type = EventLogMessageType.Error,
                                Date = DateTime.Now,
                                Caption = step.Caption,
                                Message = $"Failed to load module settings. Exception: {ex.Message}"
                            });
                        }));
                        step.State = MigrationStepState.Error;
                        continue;
                    }

                    //var workflow = new DefaultMigrationWorkflow(SqlConnectionSettings.ConnectionString(), step.Route,  MigrationSettings, moduleSettings, WorkContext, Logger, new JsonSerilizer())
                    //var workflow = new BaseMigrationWorkflow(SqlConnectionSettings.ConnectionString(), MigrationSettings, moduleSettings, new WorkContext(), Logger)
                    var workflow = new SupportCentralMigrationWorkflow(SqlConnectionSettings.ConnectionString(), step.Route, MigrationSettings, moduleSettings, WorkContext, Logger, new JsonSerilizer())
                    {
                        WorkflowAction = mess =>
                        {
                            Logger.Info($"Object: {step.Caption}. {mess}");
                            Application.Current.Dispatcher.Invoke((Action)(() =>
                            {
                                Logs.Add(new EventLogMessage()
                                {
                                    Type = EventLogMessageType.Info,
                                    Date = DateTime.Now,
                                    Caption = step.Caption,
                                    Message = $"{mess}"
                                });
                            }));
                        },
                        WorkflowActionSuccess = mess =>
                        {
                            Logger.Info($"Object: {step.Caption}. {mess}");
                            Application.Current.Dispatcher.Invoke((Action)(() =>
                            {
                                Logs.Add(new EventLogMessage()
                                {
                                    Type = EventLogMessageType.Success,
                                    Date = DateTime.Now,
                                    Caption = step.Caption,
                                    Message = $"{mess}"
                                });
                            }));
                            step.State = MigrationStepState.Success;
                        },
                        WorkflowActionError = (mess, ex) =>
                        {
                            Logger.Error($"Object: {step.Caption}. {mess}", ex);
                            Application.Current.Dispatcher.Invoke((Action)(() =>
                            {
                                Logs.Add(new EventLogMessage()
                                {
                                    Type = EventLogMessageType.Error,
                                    Date = DateTime.Now,
                                    Caption = step.Caption,
                                    Message = ex != null ? $"{mess} Exception: {ex.Message}" : $"{mess}"
                                });
                            }));
                            step.State = MigrationStepState.Error;
                        }
                    };

                    workflow.Run();
                }

                Application.Current.Dispatcher.Invoke((Action)(() =>
                {
                    MigrationIsCompleted = true;
                }));

            };
            bg.RunWorkerAsync();
        }

        public override async Task<bool> CheckBeforeGoToNext()
        {
            await Task.Run(() => { });
            return true;
        }

        public override async Task<bool> GoToNext()
        {
            await Task.Run(() =>
            {
                //todo save
            });
            Application.Current.Shutdown();
            return true;
        }

        public override void Cancel()
        {
            Application.Current.Shutdown();
        }
    }
}
