﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Boss.Migration;
using Boss.MigrationCore.Enumerators;

namespace SupportCentral.MigrationTool
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Mutex _myMutex;

        private void Application_Startup(object sender, StartupEventArgs e)
        {

            Constants.CurrentMigrationTarget = MigrationTarget.BossSupportCentral;

            bool isNewInstance = false;
            _myMutex = new Mutex(true, "BSCMigrationTool", out isNewInstance);

            if (!isNewInstance)
            {
                App.Current.Shutdown();
            }

            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-IN");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-IN");
        }
    }
}
