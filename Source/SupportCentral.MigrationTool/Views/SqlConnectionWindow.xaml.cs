﻿using System.Windows;
using Boss.BaseControls.Controls;
using Boss.Migration;
using Boss.Migration.Context;
using Boss.Migration.Logging;
using Boss.Migration.Settings;
using Boss.MigrationCore.Common.Serilizer;
using SupportCentral.MigrationTool.ViewModels;

namespace SupportCentral.MigrationTool.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class SqlConnectionWindow : AdvancedWindow
    {
        public SqlConnectionWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SupportCentral.MigrationTool.Views.MigrationSettingsWindow main = new SupportCentral.MigrationTool.Views.MigrationSettingsWindow();
            Application.Current.MainWindow = main;
            this.Close();
            main.Show();
        }

        private void AdvancedWindow_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext = new SqlConnectionViewModel(new Logger(Constants.CurrentMigrationTarget), new BaseWorkContext(Constants.CurrentMigrationTarget), new SqlConnectionSettings(new JsonFileSerilizer()), this);
        }
    }
}
