﻿using System;
using Boss.BaseControls.Models;
using Boss.GuiCore.Enumerators;

namespace Boss.GuiCore.Models
{
    public class EventLogMessage: BaseModel
    {
        public EventLogMessageType Type { get; set; }

        public DateTime Date { get; set; }

        public string Caption { get; set; }

        public string Message { get; set; }
    }
}
