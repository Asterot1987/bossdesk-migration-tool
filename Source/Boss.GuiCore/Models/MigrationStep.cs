﻿using System;
using Boss.BaseControls.Models;
using Boss.GuiCore.Enumerators;

namespace Boss.GuiCore.Models
{
    public class MigrationStep : BaseModel
    {
        private MigrationStepState _state;
        private string _caption;

        public Guid Guid { get; set; }
        public string Route { get; set; }
        public int Index { get; set; }

        public MigrationStepState State
        {
            get { return _state; }
            set
            {
                _state = value;
                OnPropertyChanged();
            }
        }

        public string Caption
        {
            get { return _caption; }
            set
            {
                _caption = value;
                OnPropertyChanged();
            }
        }
    }
}
