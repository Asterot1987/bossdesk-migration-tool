﻿using System.Threading.Tasks;
using System.Windows.Input;
using Boss.BaseControls.Abstractions;
using Boss.BaseControls.Models;
using Boss.GuiCore.ViewModels.Abstractions;
using Boss.MigrationCore.Context;
using Boss.MigrationCore.Logging;

namespace Boss.GuiCore.ViewModels.BaseImplementations
{
    public abstract class BaseWindowViewModel<T> : BaseModel, IWindowViewModel, IValidation<T>
    {
        private bool _isBusy = false;
        private string _busyContent;
        private bool _isCorrect = true;
        private T _errorData;

        public ILog Logger { get; set; }

        public IWorkContext WorkContext;

        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
                if (!_isBusy) BusyContent = "";
            }
        }

        public string BusyContent
        {
            get { return _busyContent; }
            set
            {
                _busyContent = value; 
                OnPropertyChanged();
            }
        }

        public bool IsCorrect
        {
            get { return _isCorrect; }
            set
            {
                _isCorrect = value;
                OnPropertyChanged();
            }
        }

        public T ErrorData
        {
            get { return _errorData; }
            set
            {
                _errorData = value;
                OnPropertyChanged();
            }
        }


        public ICommand GoToNextCommand { get; set; }

        public ICommand GoToBackCommand { get; set; }

        public ICommand CancelCommand { get; set; }


        protected BaseWindowViewModel(ILog logger, IWorkContext workContext)
        {
            Logger = logger;
            WorkContext = workContext;
            InitCommands();
        }

        public virtual void InitCommands()
        {
            GoToNextCommand = new Command(async o =>
            {
                
                if (await CheckBeforeGoToNext())
                {
                    await GoToNext();
                }
            });           

            CancelCommand = new Command(o => Cancel());
        }

        public virtual bool InitData()
        {
            return true;
        }

        public abstract Task<bool> CheckBeforeGoToNext();

        public abstract Task<bool> GoToNext();

        public abstract void Cancel();

        
    }
}
