﻿using System.Threading.Tasks;
using Boss.BaseControls.Abstractions;

namespace Boss.GuiCore.ViewModels.Abstractions
{
    public interface IWindowViewModel : IViewModel<bool>, IBusy
    {
        Task<bool> CheckBeforeGoToNext();

        Task<bool> GoToNext();

        void Cancel();
    }
}
