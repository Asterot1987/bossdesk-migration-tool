﻿namespace Boss.GuiCore.Enumerators
{
    public enum EventLogMessageType
    {
        Info,
        Error,
        Success
    }
}
