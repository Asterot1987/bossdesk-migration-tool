﻿namespace Boss.GuiCore.Enumerators
{
    public enum MigrationStepState
    {
        Wait,
        InProgress,
        Error,
        Success
    }
}
