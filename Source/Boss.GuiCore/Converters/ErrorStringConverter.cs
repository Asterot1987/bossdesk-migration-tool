﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Boss.MigrationCore.Common.Exceptions;

namespace Boss.GuiCore.Converters
{
    public class ErrorStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FontWeight f = FontWeights.Light;

            var type = value as ErrorSubStringType? ?? ErrorSubStringType.Caption;

            switch (type)
            {
                case ErrorSubStringType.Caption:
                    f = FontWeights.Light;
                    break;
                case ErrorSubStringType.Object:
                    f = FontWeights.SemiBold;
                    break;
            }

            return f;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
