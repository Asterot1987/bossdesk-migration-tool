﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Boss.GuiCore.Enumerators;

namespace Boss.GuiCore.Converters
{
    public class StepStateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var vis = Visibility.Collapsed;

            MigrationStepState par;

            if (value is MigrationStepState && Enum.TryParse(parameter.ToString(), true, out par))
            {
                vis = par == (MigrationStepState)value ? Visibility.Visible : Visibility.Collapsed;

            }
            return vis;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return MigrationStepState.Wait;
        }
    }
}
