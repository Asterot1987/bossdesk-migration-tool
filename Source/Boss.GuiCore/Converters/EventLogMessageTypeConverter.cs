﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Boss.GuiCore.Enumerators;

namespace Boss.GuiCore.Converters
{
    public class EventLogMessageTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var vis = Visibility.Collapsed;

            EventLogMessageType par;

            if (value is EventLogMessageType && Enum.TryParse(parameter.ToString(), true, out par))
            {
                vis = par == (EventLogMessageType)value ? Visibility.Visible : Visibility.Collapsed;

            }
            return vis;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return EventLogMessageType.Info;
        }
    }
}
