﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Boss.Migration.Models;

namespace Boss.GuiCore.Converters
{
    public class GeneratorVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var vis = Visibility.Collapsed;
            try
            {
                switch (parameter.ToString())
                {
                    case "ShowTransformType":
                        vis = (TransformType)value == TransformType.Default ? Visibility.Collapsed : Visibility.Visible;
                        break;
                    case "ShowMoreThanZero":
                        vis = System.Convert.ToInt32(value) == 0 ? Visibility.Collapsed : Visibility.Visible;
                        break;

                    case "ShowDefaultTransformType":
                        vis = ((TransformType)value == TransformType.Default || (TransformType)value == TransformType.HierarchicalTransform || (TransformType)value == TransformType.HashCode) ? Visibility.Visible : Visibility.Collapsed;
                        break;

                    case "ShowReplaceWithCacheTransformType":
                        vis = ((TransformType)value == TransformType.DefaultReplaceWithCacheObject || (TransformType)value == TransformType.HierarchicalTransformAndReplaceWithCacheObject) ? Visibility.Visible : Visibility.Collapsed;
                        break;

                    case "ShowSplitTransformType":
                        vis = ((TransformType)value == TransformType.SplitTransform || (TransformType)value == TransformType.SplitAndUuidTransform) ? Visibility.Visible : Visibility.Collapsed;
                        break;
                }
            }
            catch (Exception)
            {
            }
            return vis;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return false;
        }
    }
}
