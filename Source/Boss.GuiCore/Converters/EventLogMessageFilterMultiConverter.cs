﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using Boss.GuiCore.Enumerators;

namespace Boss.GuiCore.Converters
{
    public class EventLogMessageFilterMultiConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            Visibility vis = Visibility.Collapsed;

            try
            {
                if (values[1] != DependencyProperty.UnsetValue)
                {
                    vis = ((bool) values[0] && (EventLogMessageType) values[1] == EventLogMessageType.Error) ||
                          !(bool) values[0]
                        ? Visibility.Visible
                        : Visibility.Collapsed;
                }
                else
                {
                    vis = Visibility.Collapsed;
                }
            }
            catch
            {
            }
            
            return vis;
        }
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
