﻿using System.Windows;
using Boss.BaseControls.Controls;

namespace BossDesk.MigrationTool.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MigrationProgressWindow : AdvancedWindow
    {
        public MigrationProgressWindow()
        {
            InitializeComponent();
            this.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            MigrationProgressWindow main = new MigrationProgressWindow();
            Application.Current.MainWindow = main;
            this.Close();
            main.Show();
        }

        private void AdvancedWindow_Loaded(object sender, RoutedEventArgs e)
        {
        }
    }
}
