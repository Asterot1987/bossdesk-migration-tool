﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Boss.BaseControls.Controls;
using Boss.BaseControls.Models;
using Boss.GuiCore.ViewModels.BaseImplementations;
using Boss.Migration;
using Boss.Migration.Context;
using Boss.Migration.Logging;
using Boss.Migration.Settings;
using Boss.MigrationCore.Common;
using Boss.MigrationCore.Common.Serilizer;
using Boss.MigrationCore.Context;
using Boss.MigrationCore.Enumerators;
using Boss.MigrationCore.Logging;
using BossDesk.MigrationTool.Views;

namespace BossDesk.MigrationTool.ViewModels
{
    public class SqlConnectionViewModel : BaseWindowViewModel<string>
    {
        private IWorkContext _projectSettingsWorkContext;// = new BaseWorkContext(Constants.CurrentMigrationTarget);
        private AdvancedWindow _win;
        private ISettings _settings;

        public ISettings Settings
        {
            get { return _settings; }
            set
            {
                _settings = value;
                OnPropertyChanged();
            }
        }

        public MigrationProjectsSettings ProjectsSettings { get; set; }

        public ICommand ReadConnectionSettingsCommand { get; set; }

        public SqlConnectionViewModel(ILog logger, IWorkContext projectWorkContext, ISettings settings, AdvancedWindow win) : base(logger, projectWorkContext)
        {
            _projectSettingsWorkContext = projectWorkContext;
            ProjectsSettings = new MigrationProjectsSettings(new JsonFileSerilizer());
            Settings = settings;
            _win = win;
            InitData();
        }

        public override bool InitData()
        {
            bool res = true;
            IsBusy = true;
            BusyContent = Properties.Resources.BusyContent_DataLoading;

            //migrationProjectSettings
            res = ReadSettings(ProjectsSettings, _projectSettingsWorkContext, "ProjectsSettings");

            //ConnectionSettings
            WorkContext = new CustomWorkContext(ProjectsSettings.LastProject.Value.ToString(), Constants.CurrentMigrationTarget);
            res = res && ReadSettings(Settings, WorkContext, "ConnectionSettings");

            IsBusy = false;
            return res;
        }

        private bool ReadSettings(ISettings settings, IWorkContext workContext, string settingsName = null)
        {
            bool res = true;

            try
            {
                Logger.Info($"Object: {settingsName ?? "Settings"}. Method: [InitData]. Settings reading started");
                settings.Read(Path.Combine(workContext.SettingsDirectory, $"{settings.WorkflowId}.json"));
                Logger.Info($"Object: {settingsName ?? "Settings"}. Method: [InitData]. Settings reading completed");
            }
            catch (Exception ex)
            {
                Logger.Error($"Object: {settingsName ?? "Settings"}. Method: [InitData]. Settings reading error", ex);
                res = false;
            }

            return res;
        }

        private bool WriteSettings(ISettings settings, string settingsName = null, string method = null)
        {
            var result = true;
            try
            {
                Logger.Info($"Object: {settingsName ?? "Settings"}. Method: [{method ?? "-"}]. Settings saving started");
                settings.Write();
                Logger.Info($"Object: {settingsName ?? "Settings"}. Method: [{method ?? "-"}]. Settings saving completed");
            }
            catch (Exception ex)
            {
                Logger.Error($"Object: {settingsName ?? "Settings"}. Method: [{method ?? "-"}]. Settings saving error", ex);
                result = false;
            }
            return result;
        }

        public override void InitCommands()
        {
            base.InitCommands();

            ReadConnectionSettingsCommand = new Command(o =>
            {
                WorkContext = new CustomWorkContext(ProjectsSettings.LastProject.Value.ToString(), Constants.CurrentMigrationTarget);
                ReadSettings(Settings, WorkContext, "ConnectionSettings");
            });
        }

        public override async Task<bool> CheckBeforeGoToNext()
        {
            IsBusy = true;
            IsCorrect = true;
            BusyContent = Properties.Resources.IsBusyConnectionToDatabase;
            ErrorData = "";

            await Task.Run(() =>
            {
                try
                {
                    Logger.Info($"Object: SQLConnection. Method: [CheckBeforeGoToNext]. Validation started");
                    SqlConnection sqlConnection = new SqlConnection((Settings as SqlConnectionSettings).ConnectionString());
                    sqlConnection.Open();
                    if (sqlConnection.State != ConnectionState.Open) IsCorrect = false;
                    sqlConnection.Close();
                }
                catch (Exception ex)
                {
                    Logger.Error($"Object: SQLConnection. Method: [CheckBeforeGoToNext]. Validation error", ex);
                    IsCorrect = false;
                }

            });

            Logger.Info($"Object: SQLConnection. Method: [CheckBeforeGoToNext]. Validation completed");
            IsBusy = false;
            ErrorData = IsCorrect ? "" : Properties.Resources.ErrorUnableToConnect;
            return IsCorrect;
        }

        public override async Task<bool> GoToNext()
        {
            var result = true;
            IsBusy = true;
            BusyContent = Properties.Resources.BusyContent_DataSaving;

            await Task.Run(() =>
            {
                result = WriteSettings(Settings, "ConnectionSettings", "GoToNext") &&
                         WriteSettings(ProjectsSettings, "ProjectsSettings", "GoToNext");
            });
            IsBusy = false;

            try
            {
                Application.Current.Dispatcher.Invoke((Action) (() =>
                {
                    var main = new MigrationSettingsWindow();
                    main.DataContext = new MigrationSettingsViewModel(ProjectsSettings, new Logger(Constants.CurrentMigrationTarget), WorkContext, new MigrationSettings(new JsonFileSerilizer()), main);
                    Application.Current.MainWindow = main;
                    _win.Close();
                    main.Show();
                }));
            }
            catch (Exception ex)
            {
                Logger.Error($"Object: SQLConnection. Method: [GoToNext]. Go to next step error", ex);
                result = false;
            }

            return result;
        }

        public override void Cancel()
        {
            Application.Current.Shutdown();
        }

    }
}
