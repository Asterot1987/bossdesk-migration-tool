﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Boss.BaseControls.Controls;
using Boss.BaseControls.Models;
using Boss.GuiCore.ViewModels.BaseImplementations;
using Boss.Migration;
using Boss.Migration.Context;
using Boss.Migration.Logging;
using Boss.Migration.Settings;
using Boss.MigrationCore.Common;
using Boss.MigrationCore.Common.Exceptions;
using Boss.MigrationCore.Common.Serilizer;
using Boss.MigrationCore.Context;
using Boss.MigrationCore.Enumerators;
using Boss.MigrationCore.Logging;
using BossDesk.MigrationTool.Views;

namespace BossDesk.MigrationTool.ViewModels
{
    public class MigrationSettingsViewModel : BaseWindowViewModel<string>
    {
        private AdvancedWindow _win;

        private ISettings _migrationSettings;
        private OperationResult _validationResult;
        private double _modulesListHeight = 210; //40 * 5 + 10
        private double _windowHeight = 570; //list + 370

        public ISettings MigrationSettings
        {
            get { return _migrationSettings; }
            set
            {
                _migrationSettings = value; 
                OnPropertyChanged();
            }
        }

        public string Header { get; set; }

        public MigrationProjectsSettings ProjectsSettings { get; set; }

        public OperationResult ValidationResult
        {
            get { return _validationResult; }
            set
            {
                _validationResult = value;
                OnPropertyChanged();
            }
        }

        public DateTime MaxDateTime { get; set; } = DateTime.Now;

        public double ModulesListHeight
        {
            get { return _modulesListHeight; }
            set
            {
                _modulesListHeight = value;
                OnPropertyChanged();
            }
        }

        public double WindowHeight
        {
            get { return _windowHeight; }
            set
            {
                _windowHeight = value;
                OnPropertyChanged();
            }
        }

        public ICommand ResetValidation { get; set; }

        public MigrationSettingsViewModel(MigrationProjectsSettings projectsSettings, ILog logger, IWorkContext workContext, ISettings settings, AdvancedWindow win) : base(logger, workContext)
        {
            WorkContext = workContext;
            ProjectsSettings = projectsSettings;
            MigrationSettings = settings;
            Header = $"{ProjectsSettings?.LastProject?.Value} Data Migration Modules";
            _win = win;
            InitData();
        }

        public override bool InitData()
        {
            bool res = true;
            IsBusy = true;
            BusyContent = Properties.Resources.BusyContent_DataLoading;

            try
            {
                Logger.Info($"Object: SQLConnection. Method: [InitData]. Settings reading started");
                MigrationSettings.Read(Path.Combine(WorkContext.SettingsDirectory, $"{MigrationSettings.WorkflowId}.json"));

                var settings = MigrationSettings as MigrationSettings;

                if (settings?.MigrationModules != null)
                {
                    foreach (var module in settings.MigrationModules)
                    {
                        if (string.IsNullOrEmpty(module.Route))
                        {
                            //module.Caption += " [API not set]";
                            module.IsEnabled = false;
                        }
                    }

                    //set size
                    int modulesCount = (MigrationSettings as MigrationSettings).MigrationModules.Count;
                    int rowCount = (modulesCount % 2 == 0 ? modulesCount : modulesCount + 1) / 2;
                    ModulesListHeight = rowCount * 40 + 10;
                    WindowHeight = ModulesListHeight + 410;
                }

                Logger.Info($"Object: SQLConnection. Method: [InitData]. Settings reading completed");               
            }
            catch (Exception ex)
            {
                Logger.Error($"Object: SQLConnection. Method: [InitData]. Settings reading error", ex);
                res = false;
            }

            IsBusy = false;
            return res;
        }

        public override void InitCommands()
        {
            base.InitCommands();
            ResetValidation = new Command(o => ValidationResult = new OperationResult());

            GoToBackCommand = new Command(o =>
            {
                try
                {
                    Application.Current.Dispatcher.Invoke((Action)(() =>
                    {
                        var main = new SqlConnectionWindow();
                        main.DataContext = new SqlConnectionViewModel(new Logger(Constants.CurrentMigrationTarget), new BaseWorkContext(Constants.CurrentMigrationTarget), new SqlConnectionSettings(new JsonFileSerilizer()), main);
                        Application.Current.MainWindow = main;
                        _win.Close();
                        main.Show();
                    }));
                }
                catch (Exception ex)
                {
                    Logger.Error($"Object: MigrationSettingsWindow. Method: [GoToBack]. Go to back step error", ex);
                }
            });
        }

        public override async Task<bool> CheckBeforeGoToNext()
        {
            IsBusy = true;
            BusyContent = Properties.Resources.BusyContent_DataChecking;
            OperationResult res = new OperationResult();

            await Task.Run(() =>
            {
                try
                {
                    Logger.Info($"Object: Migration Settings. Method: [CheckBeforeGoToNext]. Validation started");
                    res = MigrationSettings.Validate();
                    Logger.Info($"Object: Migration Settings. Method: [CheckBeforeGoToNext]. Validation completed");
                }
                catch (Exception ex)
                {
                    Logger.Error($"Object: Migration Settings. Method: [CheckBeforeGoToNext]. Validation error", ex);
                    res.Result = false;
                }

            });

            IsBusy = false;
            ValidationResult = res;
            return res.Result;
        }

        public override async Task<bool> GoToNext()
        {
            var result = true;
            IsBusy = true;
            BusyContent = Properties.Resources.BusyContent_DataSaving;

            await Task.Run(() =>
            {
                try
                {
                    Logger.Info($"Object: Migration Settings. Method: [GoToNext]. Settings saving started");
                    MigrationSettings.Write();
                    Logger.Info($"Object: Migration Settings. Method: [GoToNext]. Settings saving completed");
                }
                catch (Exception ex)
                {
                    Logger.Error($"Object: Migration Settings. Method: [GoToNext]. Settings saving error", ex);
                    result = false;
                }
            });
            IsBusy = false;

            try
            {
                Application.Current.Dispatcher.Invoke((Action) (() =>
                {
                    var main = new MigrationProgressWindow();
                    main.DataContext = new MigrationProgressViewModel(ProjectsSettings, new Logger(Constants.CurrentMigrationTarget), WorkContext,
                        new SqlConnectionSettings(new JsonFileSerilizer()),
                        new MigrationSettings(new JsonFileSerilizer()),
                        main);
                    Application.Current.MainWindow = main;
                    _win.Close();
                    main.Show();
                }));

            }
            catch (Exception ex)
            {
                Logger.Error($"Object: Migration Settings. Method: [GoToNext]. Go to next step error", ex);
                result = false;
            }

            return result;
        }

        public override void Cancel()
        {
            Application.Current.Shutdown();
        }
    }
}
