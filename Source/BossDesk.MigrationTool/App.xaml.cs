﻿using System.Globalization;
using System.Threading;
using System.Windows;
using Boss.Migration;
using Boss.MigrationCore.Enumerators;

namespace BossDesk.MigrationTool
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Mutex _myMutex;

        private void Application_Startup(object sender, StartupEventArgs e)
        {

            Constants.CurrentMigrationTarget = MigrationTarget.BossDesk;

            bool isNewInstance = false;
            _myMutex = new Mutex(true, "BOSSDeskMigrationTool", out isNewInstance);

            if (!isNewInstance)
            {
                App.Current.Shutdown();
            }

            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-IN");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-IN");
        }
    }
}
