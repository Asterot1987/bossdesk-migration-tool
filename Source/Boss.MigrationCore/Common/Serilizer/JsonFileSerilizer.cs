﻿using System.IO;
using Boss.MigrationCore.AccessControl;

namespace Boss.MigrationCore.Common.Serilizer
{
    public class JsonFileSerilizer : JsonSerilizer, IFileSerilizer
    {
        public TData Read<TData>(string filePath)
        {

            if (string.IsNullOrEmpty(filePath) || !File.Exists(filePath)) return default(TData);
            using (var file = File.OpenText(filePath))
            {
                string readedData = file.ReadToEnd();
                var result = Deserilize<TData>(readedData);
                file.Close();
                return result;
            }
        }

        public string Write<TData>(string filePath, TData data)
        {
            if (string.IsNullOrEmpty(filePath)) return "Error: FilePath can't be null";
            string result = "";
            using (var file = File.CreateText(filePath))
            {
                file.AutoFlush = true;
                string writeData = Serilize(data);
                file.WriteLine(writeData);
                file.Close();
            }
            using (var sac = new SetAccessControl())
            {
                sac.SetFileAccessControl(filePath);
            }
            return result;
        }
    }
}
