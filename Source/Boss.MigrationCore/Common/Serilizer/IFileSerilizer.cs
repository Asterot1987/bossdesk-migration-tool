﻿namespace Boss.MigrationCore.Common.Serilizer
{
    public interface IFileSerilizer
    {
        TData Read<TData>(string filePath);
        string Write<TData>(string filePath, TData data);
    }
}
