﻿namespace Boss.MigrationCore.Common.Serilizer
{
    public interface ISerilizer
    {
        string Serilize<TData>(TData item);

        TData Deserilize<TData>(string dataToSerilizer);
    }
}
