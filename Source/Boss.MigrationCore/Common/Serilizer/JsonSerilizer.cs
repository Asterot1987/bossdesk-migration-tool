﻿using System.Runtime.Serialization.Formatters;
using Newtonsoft.Json;

namespace Boss.MigrationCore.Common.Serilizer
{
    public class JsonSerilizer : ISerilizer
    {
        public string Serilize<TData>(TData item)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(item, Formatting.Indented,
                                    new JsonSerializerSettings
                                    {
                                        TypeNameHandling = TypeNameHandling.Auto,
                                        TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple
                                    });
        }

        public TData Deserilize<TData>(string dataToSerilizer)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<TData>(dataToSerilizer);
        }
    }
}
