﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Boss.MigrationCore.Common.Exceptions;
using Boss.MigrationCore.Common.Serilizer;
using Boss.MigrationCore.Properties;

namespace Boss.MigrationCore.Common
{
    public class Settings : ISettings, INotifyPropertyChanged
    {

        protected ISerilizer Serilizer;
        private Guid _workflowId;

        public virtual Guid WorkflowId
        {
            get { return _workflowId; }

            set { _workflowId = value; }
        }

        public Settings(ISerilizer serilizer)
        {
            Serilizer = serilizer;

        }

        public virtual void Read(string data)
        {
        }

        public virtual string Write()
        {
            return Serilizer.Serilize(this);
        }

        public virtual OperationResult Validate()
        {
            return new OperationResult {Result = true};
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
