﻿using System.Collections.Generic;

namespace Boss.MigrationCore.Common.Exceptions
{
    public class OperationResult
    {
        public bool Result { get; set; } = true;

        public List<ErrorString> Errors { get; set; } = new List<ErrorString>();
    }

    public class ErrorSubString
    {
        public ErrorSubStringType Type { get; set; }

        public string Text { get; set; }

        public ErrorSubString()
        {
        }

        public ErrorSubString(ErrorSubStringType type, string text)
        {
            Type = type;
            Text = text;
        }
    }

    public class ErrorString
    {
        public List<ErrorSubString> SubStrings { get; set; }
    }

    public enum ErrorSubStringType
    {
        Caption,
        Object
    }
}
