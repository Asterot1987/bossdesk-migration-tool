﻿using System;
using Boss.MigrationCore.Common.Exceptions;

namespace Boss.MigrationCore.Common
{
    public interface ISettings
    {
        Guid WorkflowId { get; set; }

        void Read(string data);
        string Write();

        OperationResult Validate();
    }
}
