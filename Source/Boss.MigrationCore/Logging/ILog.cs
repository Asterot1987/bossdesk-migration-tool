﻿using System;
using System.Collections.Generic;

namespace Boss.MigrationCore.Logging
{

    public interface ILog
    {
        void Warning(string message);
        void Info(string message);
        void Error(string message);

        void Debug(string message);

        void Error(string message, Exception exception);

        List<LogModel> GetLogs();

    }

}