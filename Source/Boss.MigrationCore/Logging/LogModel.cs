﻿using System;

namespace Boss.MigrationCore.Logging
{
    public class LogModel
    {
        public string ModuleName { get; set; }

        public DateTime Date { get; set; }

        public string Message { get; set; }
    }
}
