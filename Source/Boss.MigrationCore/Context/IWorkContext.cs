﻿using Boss.MigrationCore.Enumerators;

namespace Boss.MigrationCore.Context
{
    public interface IWorkContext
    {
        MigrationTarget MigrationTarget { get; set; }
        string BaseDirectory { get; }
        string LogDirectory { get; }
        string SettingsDirectory { get; }
        string MigrationModulesSettingsDirectory { get; }
        string DataDirectory { get; }
    }
}
