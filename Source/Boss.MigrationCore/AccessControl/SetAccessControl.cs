﻿using System;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;

namespace Boss.MigrationCore.AccessControl
{
    public class SetAccessControl : ISetAccessControl, IDisposable
    {
        //Установка пользовательского контроля файла
        public void SetFileAccessControl(string fileName)
        {
            try
            {
                FileInfo file = new FileInfo(fileName);
                FileSecurity access = file.GetAccessControl();
                SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.AuthenticatedUserSid, null);
                access.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.FullControl, AccessControlType.Allow));
                file.SetAccessControl(access);
            }
            catch (Exception)
            {
                //throw;
            }
        }

        //Установка пользовательского контроля каталога
        public void SetDirectoryAccessControl(string directoryName)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(directoryName);
                DirectorySecurity access = dir.GetAccessControl();
                SecurityIdentifier everyone = new SecurityIdentifier(WellKnownSidType.AuthenticatedUserSid, null);
                access.AddAccessRule(new FileSystemAccessRule(everyone, FileSystemRights.FullControl, AccessControlType.Allow));
                dir.SetAccessControl(access);
            }
            catch (Exception)
            {
                //throw;
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
