﻿namespace Boss.MigrationCore.AccessControl
{
    public interface ISetAccessControl
    {
        void SetFileAccessControl(string fileName);
        void SetDirectoryAccessControl(string directoryName);
    }
}
