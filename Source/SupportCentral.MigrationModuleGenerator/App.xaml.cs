﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Boss.Migration;
using Boss.MigrationCore.Enumerators;

namespace SupportCentral.MigrationModuleGenerator
{
    public partial class App : Application
    {
        private Mutex _myMutex;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Constants.CurrentMigrationTarget = MigrationTarget.BossSupportCentral;

            bool aIsNewInstance = false;
            _myMutex = new Mutex(true, "BSCMigrationToolGenerator", out aIsNewInstance);

            if (!aIsNewInstance)
            {
                App.Current.Shutdown();
            }
        }
    }
}
