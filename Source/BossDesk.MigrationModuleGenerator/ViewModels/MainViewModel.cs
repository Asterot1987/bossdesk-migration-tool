﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Boss.BaseControls.Abstractions;
using Boss.BaseControls.Controls;
using Boss.BaseControls.Models;
using Boss.Migration;
using Boss.Migration.Context;
using Boss.Migration.Helper;
using Boss.Migration.Logging;
using Boss.Migration.Models;
using Boss.Migration.Settings;
using Boss.MigrationCore.Common;
using Boss.MigrationCore.Common.Serilizer;
using Boss.MigrationCore.Context;
using Boss.MigrationCore.Enumerators;
using Boss.MigrationCore.Logging;

namespace BossDesk.MigrationModuleGenerator.ViewModels
{
    public class MainViewModel : BaseModel, IViewModel<String>
    {
        private AdvancedWindow _win;
        private string _defaultDAteSelectorQuery = "where DATEDIFF(day, CAST('{D}' AS DATETIME), CreatedAtField) >= 0";

        private SqlConnectionSettings _sqlConnectionSettings;
        private MigrationSettings _migrationSettings;
        private IWorkContext _workContext;
        private RangeEnabledObservableCollection<MigrationModule> _modules = new RangeEnabledObservableCollection<MigrationModule>();
        private MigrationModule _currentModule;
        private ModuleField _currentField;

        public MigrationProjectsSettings ProjectsSettings { get; set; }

        public SqlConnectionSettings SqlConnectionSettings
        {
            get { return _sqlConnectionSettings; }
            set
            {
                _sqlConnectionSettings = value;
                OnPropertyChanged();
            }
        }

        public MigrationSettings MigrationSettings
        {
            get { return _migrationSettings; }
            set
            {
                _migrationSettings = value;
                OnPropertyChanged();
            }
        }

        public RangeEnabledObservableCollection<MigrationModule> Modules
        {
            get { return _modules; }
            set
            {
                _modules = value;
                OnPropertyChanged();
            }
        }

        public MigrationModule CurrentModule
        {
            get { return _currentModule; }
            set
            {
                _currentModule = value;
                OnPropertyChanged();
            }
        }

        public ModuleField CurrentField
        {
            get { return _currentField; }
            set
            {
                _currentField = value;
                OnPropertyChanged();
            }
        }

        

        private FieldRecord _newRecord = new FieldRecord();
        private RangeEnabledObservableCollection<ModuleField> _cacheFields = new RangeEnabledObservableCollection<ModuleField>();
        private string _newModuleName;

        public FieldRecord NewRecord
        {
            get { return _newRecord; }
            set
            {
                _newRecord = value;
                OnPropertyChanged();
            }
        }


        public RangeEnabledObservableCollection<ModuleField> CacheFields
        {
            get { return _cacheFields; }
            set
            {
                _cacheFields = value;
                OnPropertyChanged();
            }
        }

        public RangeEnabledObservableCollection<CheckGuidWithRouteModel> ReoderCollection { get; set; } = new RangeEnabledObservableCollection<CheckGuidWithRouteModel>();
        public bool IsReordering { get; set; }



        public bool IsNewModuleCreating { get; set; }

        public string NewModuleName
        {
            get { return _newModuleName; }
            set
            {
                _newModuleName = value;
                OnPropertyChanged();
                NewModuleNameIsCorrect = !string.IsNullOrEmpty(_newModuleName) && MigrationSettings.MigrationModules.All(x => x.Caption != _newModuleName);
            }
        }

        public bool NewModuleNameIsCorrect { get; set; }

        public ICommand CreateModuleCommand { get; set; }



        public ICommand RereadModuleCommand { get; set; }
        public ICommand CreateFieldsCommand { get; set; }
        public ICommand ReorderModulesCommand { get; set; }
        public ICommand RemoveModuleCommand { get; set; }
        public ICommand UpdateFieldsCommand { get; set; }
        public ICommand GetDefaultDateSelectorQueryCommand { get; set; }
        public ICommand AddNewRecordCommand { get; set; }
        public ICommand RemoveRecordCommand { get; set; }
        public ICommand SaveModuleCommand { get; set; }
        public ICommand GetCacheFieldsCommand { get; set; }
        public ICommand SetFieldTargetNameCommand { get; set; }
        public ICommand GoToBackCommand { get; set; }


        public MainViewModel(MigrationProjectsSettings projectsSettings, SqlConnectionSettings connectionSettings, IWorkContext workContext, AdvancedWindow win)
        {
            _win = win;
            ProjectsSettings = projectsSettings;
            SqlConnectionSettings = connectionSettings;
            _workContext = workContext;
            InitCommands();
            InitData();
        }

        public string InitData()
        {
            Modules.Clear();

            SqlConnectionSettings = new SqlConnectionSettings(new JsonFileSerilizer());
            MigrationSettings = new MigrationSettings(new JsonFileSerilizer());

            SqlConnectionSettings.Read(Path.Combine(_workContext.SettingsDirectory, $"{SqlConnectionSettings.WorkflowId}.json"));
            MigrationSettings.Read(Path.Combine(_workContext.SettingsDirectory, $"{MigrationSettings.WorkflowId}.json"));

            if (MigrationSettings.MigrationModules != null)
            {
                foreach (var module in MigrationSettings.MigrationModules)
                {
                    MigrationModule newModule = new MigrationModule()
                    {
                        Guid = module.Guid,
                        IsEnabled = module.IsEnabled,
                        Fields = new RangeEnabledObservableCollection<ModuleField>(),
                        Query = "",
                        Caption = module.Caption
                    };
                    var js = new JsonFileSerilizer();

                    var path = Path.Combine(_workContext.MigrationModulesSettingsDirectory, $"{module.Guid}.json");

                    if (File.Exists(path))
                    {
                        newModule = js.Read<MigrationModule>(path);
                    }
                    else
                    {
                        var defModulesOperations = new DefaultModulesOperations(new Logger(Constants.CurrentMigrationTarget));

                        var moduleFolder = ProjectsSettings.LastProject.Value.ToString().Replace(" ", "_");
                        moduleFolder = moduleFolder.Replace(".", "._");

                        var defModule = defModulesOperations.ReadModule(
                            $"Boss.Migration.ModulesResources.{moduleFolder}.",
                            $"{module.Guid}.json");

                        defModulesOperations.WriteModule(
                            _workContext.MigrationModulesSettingsDirectory,
                            $"{module.Guid}.json",
                            !string.IsNullOrEmpty(defModule) ? defModule : new JsonSerilizer().Serilize(newModule));

                        newModule = js.Read<MigrationModule>(path);
                    }

                    if (newModule != null)
                    {
                        newModule.Route = module.Route;
                        Modules.Add(newModule);
                    }
                }

                CurrentModule = Modules.FirstOrDefault();
            }

            return "";
        }

        public void InitCommands()
        {
            CreateModuleCommand = new Command(o =>
            {
                switch (o?.ToString())
                {
                    case "New":
                        NewModuleName = "";
                        IsNewModuleCreating = true;
                        break;

                    case "Save":

                        try
                        {
                            var newModule = new CheckGuidWithRouteModel()
                            {
                                Guid = Guid.NewGuid(),
                                Route = "",
                                Caption = NewModuleName,
                                IsEnabled = false
                            };
                            MigrationSettings.MigrationModules.Add(newModule);
                            MigrationSettings.Write();

                            var path = Path.Combine(_workContext.MigrationModulesSettingsDirectory, $"{newModule.Guid}.json");
                            var module = new MigrationModule()
                            {
                                Caption = newModule.Caption,
                                Guid = newModule.Guid,
                                Route = "",
                                IsEnabled = false,
                                Fields = new RangeEnabledObservableCollection<ModuleField>(),
                                DateSelectorQuery = "",
                                Query = "",
                                UseDateSelector = false
                            };
                            var js = new JsonFileSerilizer().Write(path, module);
                            Modules.Add(module);
                            CurrentModule = Modules.FirstOrDefault(x => x.Guid == module.Guid);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        
                        NewModuleName = "";
                        IsNewModuleCreating = false;
                        break;

                    case "Cancel":
                        IsNewModuleCreating = false;
                        NewModuleName = "";
                        break;
                }
            });

            ReorderModulesCommand = new Command(o =>
            {
                switch (o?.ToString())
                {
                    case "Reorder":
                        IsReordering = true;
                        ReoderCollection = new RangeEnabledObservableCollection<CheckGuidWithRouteModel>(MigrationSettings.MigrationModules.Select(x => new CheckGuidWithRouteModel()
                        {
                            Guid = x.Guid,
                            Route = x.Route,
                            Caption = x.Caption,
                            IsEnabled = x.IsEnabled
                        }).ToList());
                        break;

                    case "Save":
                        try
                        {
                            MigrationSettings.MigrationModules = new RangeEnabledObservableCollection<CheckGuidWithRouteModel>(ReoderCollection.Select(x => new CheckGuidWithRouteModel()
                            {
                                Guid = x.Guid,
                                Route = x.Route,
                                Caption = x.Caption,
                                IsEnabled = x.IsEnabled
                            }).ToList());
                            MigrationSettings.Write();
                            InitData();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }

                        IsReordering = false;
                        break;

                    case "Cancel":
                        IsReordering = false;
                        break;
                }
            });

            RemoveModuleCommand = new Command(o =>
            {
                MessageBoxResult messageBoxResult = System.Windows.MessageBox.Show($"Are you sure you want to delete the module [{CurrentModule.Caption}]", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    var find = MigrationSettings.MigrationModules.FirstOrDefault(x => x.Guid == CurrentModule.Guid);
                    if (find != null)
                    {
                        MigrationSettings.MigrationModules.Remove(find);
                        MigrationSettings.Write();
                        InitData();
                    }
                }
            });

            GoToBackCommand = new Command(o =>
            {
                    try
                    {
                        Application.Current.Dispatcher.Invoke((Action)(() =>
                        {
                            var main = new Views.SqlConnectionWindow();
                            main.DataContext = new SqlConnectionViewModel(new Logger(Constants.CurrentMigrationTarget), new BaseWorkContext(Constants.CurrentMigrationTarget), new SqlConnectionSettings(new JsonFileSerilizer()), main);
                            Application.Current.MainWindow = main;
                            _win.Close();
                            main.Show();
                        }));
                    }
                    catch (Exception ex)
                    {
                        //Logger.Error($"Object: SQLConnection. Method: [GoToNext]. Go to next step error", ex);
                    }
            });

            RereadModuleCommand = new Command(o =>
            {
                if (CurrentModule != null)
                {
                    var js = new JsonFileSerilizer();
                    var path = Path.Combine(_workContext.MigrationModulesSettingsDirectory, $"{CurrentModule.Guid}.json");
                    if (File.Exists(path))
                    {
                        CurrentModule = js.Read<MigrationModule>(path);

                        var findModule = MigrationSettings.MigrationModules.FirstOrDefault(x => x.Guid == CurrentModule.Guid);
                        if (findModule != null) CurrentModule.Route = findModule.Route;
                    }
                }
            });

            CreateFieldsCommand = new Command(o =>
            {
                if (CurrentModule != null)
                {
                    CurrentModule.Fields.Clear();
                    CurrentModule.Fields.InsertRange(GetModuleFields(CurrentModule.Query,
                        SqlConnectionSettings.ConnectionString()));
                }
            });

            UpdateFieldsCommand = new Command(o =>
            {
                if (CurrentModule != null)
                {
                    var sqlFields = GetModuleFields(CurrentModule.Query, SqlConnectionSettings.ConnectionString());

                    foreach (var field in sqlFields)
                    {
                        var find = CurrentModule.Fields.FirstOrDefault(f => f.SourceName == field.SourceName);
                        if (find != null)
                        {
                            field.Guid = field.Guid;
                            field.IsEnabled = find.IsEnabled;
                            field.SourceType = find.SourceType;
                            field.TargetName = find.TargetName;
                            field.TargetType = find.TargetType;
                            field.TransformType = find.TransformType;
                            field.ReplaceRecords.Clear();
                            field.ReplaceRecords.InsertRange(find.ReplaceRecords);
                        }
                    }

                    CurrentModule.Fields.Clear();
                    CurrentModule.Fields.InsertRange(sqlFields);

                    //var toAdd = sqlFields.Where(f => CurrentModule.Fields.All(oldf => oldf.SourceName != f.SourceName)).ToList();

                    //var toRemove =
                    //    CurrentModule.Fields.Where(oldf => sqlFields.All(f => oldf.SourceName != f.SourceName)).ToList();

                    //foreach (var field in toRemove)
                    //{
                    //    CurrentModule.Fields.Remove(field);
                    //}

                    //foreach (var field in toAdd)
                    //{
                    //    CurrentModule.Fields.Add(field);
                    //}

                    
                }
            });

            GetDefaultDateSelectorQueryCommand = new Command(o =>
            {
                if (CurrentModule != null) CurrentModule.DateSelectorQuery = _defaultDAteSelectorQuery;
            });

            AddNewRecordCommand = new Command(o =>
            {
                FieldRecord item = new FieldRecord()
                {
                    SourceValue = NewRecord.SourceValue,
                    TargetValue = NewRecord.TargetValue
                };

                if (!string.IsNullOrEmpty(item.SourceValue) || !string.IsNullOrEmpty(item.TargetValue))
                {
                    CurrentField?.ReplaceRecords?.Add(item);
                    NewRecord = new FieldRecord();
                }
            });

            RemoveRecordCommand = new Command(o =>
            {
                CurrentField.ReplaceRecords.Remove(o as FieldRecord);
            });

            SaveModuleCommand = new Command(o =>
            {
                if (CurrentModule != null)
                {
                    var js = new JsonFileSerilizer();
                    var path = Path.Combine(_workContext.MigrationModulesSettingsDirectory, $"{CurrentModule.Guid}.json");
                    js.Write(path, CurrentModule);

                    var findModule = MigrationSettings.MigrationModules.FirstOrDefault(x => x.Guid == CurrentModule.Guid);
                    if (findModule != null) findModule.Route = CurrentModule.Route;
                    MigrationSettings.Write();
                }
            });

            GetCacheFieldsCommand = new Command(o =>
            {
                if (CurrentField != null)
                {
                    var findModule =
                        Modules.FirstOrDefault(x => x.Guid == CurrentField.ReplaceTransformObject.ModuleGuid);
                    if (findModule != null)
                    {
                        CacheFields.Clear();
                        CacheFields.InsertRange(findModule.Fields.Where(f => f.IsCacheObject));
                    }
                }
            });

            SetFieldTargetNameCommand = new Command(o =>
            {
                if (o != null && CurrentField != null)
                {
                    CurrentField.TargetName = o.ToString();
                    if (o.ToString() == Constants.LegacyId)
                    {
                        CurrentField.IsCacheObject = true;
                    }
                }
            });
        }

        public IEnumerable<ModuleField> GetModuleFields(string queryString, string connectionString)
        {
            var res = new List<ModuleField>();
            try
            {
                using (var connection = new SqlConnection(connectionString))
                using (var command = new SqlCommand(queryString, connection))
                {
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                        if (reader.HasRows)
                        {
                            var columns = Enumerable.Range(0, reader.FieldCount).Select(r =>
                            {
                                var column = new ModuleField()
                                {
                                    IsEnabled = true,
                                    SourceName = reader.GetName(r),
                                    SourceType = reader.GetFieldType(r),
                                    TargetName = reader.GetName(r),
                                    TargetType = reader.GetFieldType(r),
                                };
                                return column;
                            });
                            res.AddRange(columns);
                        }
                }
                return res;

            }
            catch (System.Exception)
            {
                //
            }
            return res;
        }

    }
}
