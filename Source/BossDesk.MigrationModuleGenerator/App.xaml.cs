﻿using System.Threading;
using System.Windows;
using Boss.Migration;
using Boss.MigrationCore.Enumerators;

namespace BossDesk.MigrationModuleGenerator
{



    public partial class App : Application
    {
        private Mutex _myMutex;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Constants.CurrentMigrationTarget = MigrationTarget.BossDesk;

            bool aIsNewInstance = false;
            _myMutex = new Mutex(true, "BOSSDeskMigrationToolGenerator", out aIsNewInstance);

            if (!aIsNewInstance)
            {
                App.Current.Shutdown();
            }
        }
    }
}
