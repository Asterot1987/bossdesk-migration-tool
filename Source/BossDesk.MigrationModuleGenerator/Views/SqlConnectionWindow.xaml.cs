﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Boss.BaseControls.Controls;
using Boss.Migration;
using Boss.Migration.Context;
using Boss.Migration.Logging;
using Boss.Migration.Settings;
using Boss.MigrationCore.Common.Serilizer;
using Boss.MigrationCore.Enumerators;
using Boss.MigrationCore.Logging;
using BossDesk.MigrationModuleGenerator.ViewModels;

namespace BossDesk.MigrationModuleGenerator.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class SqlConnectionWindow : AdvancedWindow
    {
        public SqlConnectionWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Views.MainWindow main = new Views.MainWindow();
            Application.Current.MainWindow = main;
            this.Close();
            main.Show();
        }

        private void AdvancedWindow_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext = new SqlConnectionViewModel(new Logger(Constants.CurrentMigrationTarget), new BaseWorkContext(Constants.CurrentMigrationTarget), new SqlConnectionSettings(new JsonFileSerilizer()),  this);
        }

        private void WatermarkTextBox_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            char c = e.Text[e.Text.Length - 1];

            var ignore = new List<char>()
            {
                '\\', '/', ':', '*', '?', '<', '>', '|', '"'
            };

            if (ignore.Any(x => x == c))
            {
                e.Handled = true;
            }
        }
    }
}
