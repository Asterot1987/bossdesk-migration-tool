﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Boss.BaseControls.Controls;
using Boss.Migration.Models;
using BossDesk.MigrationModuleGenerator.ViewModels;

namespace BossDesk.MigrationModuleGenerator.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : AdvancedWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            this.MaxHeight = SystemParameters.MaximizedPrimaryScreenHeight;

            Style itemContainerStyle = new Style(typeof(ListBoxItem));
            itemContainerStyle.Setters.Add(new Setter(ListBoxItem.AllowDropProperty, true));
            itemContainerStyle.Setters.Add(new EventSetter(ListBoxItem.PreviewMouseLeftButtonDownEvent, new MouseButtonEventHandler(ReorderListBox_PreviewMouseLeftButtonDown)));
            itemContainerStyle.Setters.Add(new EventSetter(ListBoxItem.DropEvent, new DragEventHandler(ReorderListBox_Drop)));
            ReorderListBox.ItemContainerStyle = itemContainerStyle;
        }

        private void DockPanel_Loaded(object sender, RoutedEventArgs e)
        {
            //DataContext = new MainViewModel();
        }

        private void ReorderListBox_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (sender is ListBoxItem)
            {
                ListBoxItem draggedItem = sender as ListBoxItem;
                DragDrop.DoDragDrop(draggedItem, draggedItem.DataContext, DragDropEffects.Move);
                draggedItem.IsSelected = true;
            }
        }

        private void ReorderListBox_Drop(object sender, DragEventArgs e)
        {
            CheckGuidWithRouteModel droppedData = e.Data.GetData(typeof(CheckGuidWithRouteModel)) as CheckGuidWithRouteModel;
            CheckGuidWithRouteModel target = ((ListBoxItem)(sender)).DataContext as CheckGuidWithRouteModel;

            int removedIdx = ReorderListBox.Items.IndexOf(droppedData);
            int targetIdx = ReorderListBox.Items.IndexOf(target);

            var dc = DataContext as MainViewModel;

            if (dc != null && dc.ReoderCollection != null)
            {
                if (removedIdx < targetIdx)
                {
                    dc.ReoderCollection.Insert(targetIdx + 1, droppedData);
                    dc.ReoderCollection.RemoveAt(removedIdx);
                }
                else
                {
                    int remIdx = removedIdx + 1;
                    if (dc.ReoderCollection.Count + 1 > remIdx)
                    {
                        dc.ReoderCollection.Insert(targetIdx, droppedData);
                        dc.ReoderCollection.RemoveAt(remIdx);
                    }
                }
            }

            
        }
    }
}
