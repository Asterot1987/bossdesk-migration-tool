﻿namespace Boss.BaseControls.Models
{
    public class CheckModel : BaseModel
    {
        private bool _isEnabled;
        private string _caption;

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                _isEnabled = value;
                OnPropertyChanged();
            }
        }

        public string Caption
        {
            get { return _caption; }
            set
            {
                _caption = value; 
                OnPropertyChanged();
            }
        }

        public CheckModel()
        {
        }

        public CheckModel(string caption, bool isEnabled)
        {
            Caption = caption;
            IsEnabled = isEnabled;
        }
    }
}
