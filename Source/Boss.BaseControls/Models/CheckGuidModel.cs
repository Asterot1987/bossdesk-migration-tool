﻿using System;

namespace Boss.BaseControls.Models
{
    public class CheckGuidModel : CheckModel
    {
        public Guid Guid { get; set; }

        public CheckGuidModel()
        {
        }

        public CheckGuidModel(string guid, string caption, bool isEnabled)
        {
            Guid = Guid.Parse(guid);
            Caption = caption;
            IsEnabled = isEnabled;
        }
    }
}
