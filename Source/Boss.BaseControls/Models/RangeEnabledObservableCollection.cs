﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Boss.BaseControls.Models
{
    public class RangeEnabledObservableCollection<T> : ObservableCollection<T>
    {
        public void InsertRange(IEnumerable<T> items)
        {
            this.CheckReentrancy();
            foreach (var item in items)
                this.Items.Add(item);
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public RangeEnabledObservableCollection(IEnumerable<T> list)
            : base()
        {
            this.InsertRange(list);
        }

        public RangeEnabledObservableCollection()
            : base()
        {
        }
    }
}
