﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Boss.BaseControls.Converters
{
    public class VisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var vis = Visibility.Collapsed;
            try
            {
                switch (parameter.ToString())
                {
                    case "IntIdC":
                        vis = System.Convert.ToInt64(value) != -1 ? Visibility.Visible : Visibility.Collapsed;
                        break;
                    case "InvertIntIdC":
                        vis = System.Convert.ToInt64(value) == -1 ? Visibility.Visible : Visibility.Collapsed;
                        break;

                    case "Null":
                        vis = value == null ? Visibility.Collapsed : Visibility.Visible;
                        break;

                    case "IntH":
                        vis = value != null && System.Convert.ToInt32(value) != 0 ? Visibility.Visible : Visibility.Hidden;
                        break;
                    case "IntC":
                        vis = value != null && System.Convert.ToInt32(value) != 0 ? Visibility.Visible : Visibility.Collapsed;
                        break;
                    case "BooleanH":
                        vis = (bool)value ? Visibility.Visible : Visibility.Hidden;
                        break;
                    case "BooleanC":
                        vis = (bool)value ? Visibility.Visible : Visibility.Collapsed;
                        break;
                    case "InvertBooleanH":
                        vis = !(bool)value ? Visibility.Visible : Visibility.Hidden;
                        break;
                    case "InvertBooleanC":
                        vis = !(bool)value ? Visibility.Visible : Visibility.Collapsed;
                        break;
                    case "InvertVisibilityC":
                        vis = (Visibility)value == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;
                        break;

                    case "StringNotEmptyH":
                        vis = !string.IsNullOrEmpty(value.ToString()) ? Visibility.Visible : Visibility.Hidden;
                        break;

                    case "StringEmptyC":
                        vis = string.IsNullOrEmpty(value.ToString()) ? Visibility.Visible : Visibility.Hidden;
                        break;

                }
            }
            catch (Exception)
            {
            }
            return vis;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return false;
        }
    }
}
