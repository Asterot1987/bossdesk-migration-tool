﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Boss.BaseControls.Converters
{
    public class BooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var result = false;
            try
            {
                switch (parameter.ToString())
                {
                    case "FalseIfNull":
                        result = value != null;
                        break;

                    case "FalseIfNullorEmpty":
                        result = !string.IsNullOrEmpty(value?.ToString());
                        break;
                }
            }
            catch (Exception)
            {
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return false;
        }
    }
}
