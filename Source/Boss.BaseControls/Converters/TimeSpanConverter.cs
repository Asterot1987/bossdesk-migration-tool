﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Boss.BaseControls.Converters
{
    public class TimeSpanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var timeSpan = value as TimeSpan? ?? new TimeSpan();
            DateTime time = DateTime.Today.Add(timeSpan);
            return time.ToString("hh:mm tt");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
