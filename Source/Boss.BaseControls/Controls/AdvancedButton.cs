﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Boss.BaseControls.Controls
{
    public class AdvancedButton : Button
    {
        public static readonly DependencyProperty ImagePathProperty = DependencyProperty.Register(
            "ImagePath", typeof (Geometry), typeof (AdvancedButton), new PropertyMetadata(default(Geometry)));

        public Geometry ImagePath
        {
            get { return (Geometry) GetValue(ImagePathProperty); }
            set { SetValue(ImagePathProperty, value); }
        }

        public static readonly DependencyProperty ImageBrushProperty = DependencyProperty.Register(
            "ImageBrush", typeof (Brush), typeof (AdvancedButton), new PropertyMetadata(default(Brush)));

        public Brush ImageBrush
        {
            get { return (Brush) GetValue(ImageBrushProperty); }
            set { SetValue(ImageBrushProperty, value); }
        }

        public static readonly DependencyProperty ImageWidthProperty = DependencyProperty.Register(
            "ImageWidth", typeof (double), typeof (AdvancedButton), new PropertyMetadata(default(double)));

        public double ImageWidth
        {
            get { return (double) GetValue(ImageWidthProperty); }
            set { SetValue(ImageWidthProperty, value); }
        }

        public static readonly DependencyProperty ImageHeightProperty = DependencyProperty.Register(
            "ImageHeight", typeof (double), typeof (AdvancedButton), new PropertyMetadata(default(double)));

        public double ImageHeight
        {
            get { return (double) GetValue(ImageHeightProperty); }
            set { SetValue(ImageHeightProperty, value); }
        }

        public static readonly DependencyProperty ImageThicknessProperty = DependencyProperty.Register(
            "ImageThickness", typeof (Thickness), typeof (AdvancedButton), new PropertyMetadata(default(Thickness)));

        public Thickness ImageThickness
        {
            get { return (Thickness) GetValue(ImageThicknessProperty); }
            set { SetValue(ImageThicknessProperty, value); }
        }


        public static readonly DependencyProperty ShapeStrokeBrushProperty = DependencyProperty.Register(
            "ShapeStrokeBrush", typeof (Brush), typeof (AdvancedButton), new PropertyMetadata(default(Brush)));

        public Brush ShapeStrokeBrush
        {
            get { return (Brush) GetValue(ShapeStrokeBrushProperty); }
            set { SetValue(ShapeStrokeBrushProperty, value); }
        }

        public static readonly DependencyProperty ShapeStrokeThicknessProperty = DependencyProperty.Register(
            "ShapeStrokeThickness", typeof (double), typeof (AdvancedButton), new PropertyMetadata(1.0));

        public double ShapeStrokeThickness
        {
            get { return (double) GetValue(ShapeStrokeThicknessProperty); }
            set { SetValue(ShapeStrokeThicknessProperty, value); }
        }

        public static readonly DependencyProperty ShapeFillProperty = DependencyProperty.Register(
            "ShapeFill", typeof (Brush), typeof (AdvancedButton), new PropertyMetadata(default(Brush)));

        public Brush ShapeFill
        {
            get { return (Brush) GetValue(ShapeFillProperty); }
            set { SetValue(ShapeFillProperty, value); }
        }

        public static readonly DependencyProperty ShapeClickBrushProperty = DependencyProperty.Register(
            "ShapeClickBrush", typeof (Brush), typeof (AdvancedButton), new PropertyMetadata(default(Brush)));

        public Brush ShapeClickBrush
        {
            get { return (Brush) GetValue(ShapeClickBrushProperty); }
            set { SetValue(ShapeClickBrushProperty, value); }
        }
    }
}
