﻿using System.Windows;
using System.Windows.Controls;
using FontAwesome.WPF;

namespace Boss.BaseControls.Controls
{
    public class FontAwesomeButton : Button
    {

        public static readonly DependencyProperty IconProperty = DependencyProperty.Register(
            "Icon", typeof (FontAwesomeIcon), typeof (FontAwesomeButton), new PropertyMetadata(default(FontAwesomeIcon)));

        public FontAwesomeIcon Icon
        {
            get { return (FontAwesomeIcon) GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public static readonly DependencyProperty IconForegroundProperty = DependencyProperty.Register(
            "IconForeground", typeof (System.Windows.Media.Brush), typeof (FontAwesomeButton), new PropertyMetadata(default(System.Windows.Media.Brush)));

        public System.Windows.Media.Brush IconForeground
        {
            get { return (System.Windows.Media.Brush) GetValue(IconForegroundProperty); }
            set { SetValue(IconForegroundProperty, value); }
        }

        public static readonly DependencyProperty IconWidthProperty = DependencyProperty.Register(
            "IconWidth", typeof (double), typeof (FontAwesomeButton), new PropertyMetadata(default(double)));

        public double IconWidth
        {
            get { return (double) GetValue(IconWidthProperty); }
            set { SetValue(IconWidthProperty, value); }
        }

        public static readonly DependencyProperty IconHeightProperty = DependencyProperty.Register(
            "IconHeight", typeof (double), typeof (FontAwesomeButton), new PropertyMetadata(default(double)));

        public double IconHeight
        {
            get { return (double) GetValue(IconHeightProperty); }
            set { SetValue(IconHeightProperty, value); }
        }

        public static readonly DependencyProperty IconMarginProperty = DependencyProperty.Register(
            "IconMargin", typeof (Thickness), typeof (FontAwesomeButton), new PropertyMetadata(default(Thickness)));

        public Thickness IconMargin
        {
            get { return (Thickness) GetValue(IconMarginProperty); }
            set { SetValue(IconMarginProperty, value); }
        }
    }
}
