﻿using System.Windows;
using Xceed.Wpf.Toolkit;

namespace Boss.BaseControls.Controls
{
    public class UrlWatermarkTextBox : WatermarkTextBox
    {
        public static readonly DependencyProperty PrefixTextProperty = DependencyProperty.Register(
            "PrefixText", typeof (string), typeof (UrlWatermarkTextBox), new PropertyMetadata(default(string)));

        public string PrefixText
        {
            get { return (string) GetValue(PrefixTextProperty); }
            set { SetValue(PrefixTextProperty, value); }
        }

        public static readonly DependencyProperty SufixTextProperty = DependencyProperty.Register(
            "SufixText", typeof (string), typeof (UrlWatermarkTextBox), new PropertyMetadata(default(string)));

        public string SufixText
        {
            get { return (string) GetValue(SufixTextProperty); }
            set { SetValue(SufixTextProperty, value); }
        }

        public static readonly DependencyProperty PrefixVisibilityProperty = DependencyProperty.Register(
            "PrefixVisibility", typeof (Visibility), typeof (UrlWatermarkTextBox), new PropertyMetadata(Visibility.Visible));

        public Visibility PrefixVisibility
        {
            get { return (Visibility) GetValue(PrefixVisibilityProperty); }
            set { SetValue(PrefixVisibilityProperty, value); }
        }

        public static readonly DependencyProperty SufixVisibilityProperty = DependencyProperty.Register(
            "SufixVisibility", typeof (Visibility), typeof (UrlWatermarkTextBox), new PropertyMetadata(Visibility.Visible));

        public Visibility SufixVisibility
        {
            get { return (Visibility) GetValue(SufixVisibilityProperty); }
            set { SetValue(SufixVisibilityProperty, value); }
        }

        public static readonly DependencyProperty AdditionalBorderThicknessProperty = DependencyProperty.Register(
            "AdditionalBorderThickness", typeof (Thickness), typeof (UrlWatermarkTextBox), new PropertyMetadata(new Thickness(1,0,1,0)));

        public Thickness AdditionalBorderThickness
        {
            get { return (Thickness) GetValue(AdditionalBorderThicknessProperty); }
            set { SetValue(AdditionalBorderThicknessProperty, value); }
        }
    }
}
