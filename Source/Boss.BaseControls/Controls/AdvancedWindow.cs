﻿using System;
using System.Windows;
using System.Windows.Media;

namespace Boss.BaseControls.Controls
{
    public class AdvancedWindow : Window
    {
        public static readonly DependencyProperty TitleBarBackgroundProperty = DependencyProperty.Register(
            "TitleBarBackground", typeof (Brush), typeof (AdvancedWindow), new PropertyMetadata(Brushes.White));

        public Brush TitleBarBackground
        {
            get { return (Brush) GetValue(TitleBarBackgroundProperty); }
            set { SetValue(TitleBarBackgroundProperty, value); }
        }

        public static readonly DependencyProperty TitleBarOpacityProperty = DependencyProperty.Register(
            "TitleBarOpacity", typeof (Double), typeof (AdvancedWindow), new PropertyMetadata(1.0));

        public Double TitleBarOpacity
        {
            get { return (Double) GetValue(TitleBarOpacityProperty); }
            set { SetValue(TitleBarOpacityProperty, value); }
        }

        public static readonly DependencyProperty MinButtonVisibilityProperty = DependencyProperty.Register(
            "MinButtonVisibility", typeof (Visibility), typeof (AdvancedWindow), new PropertyMetadata(Visibility.Visible));

        public Visibility MinButtonVisibility
        {
            get { return (Visibility) GetValue(MinButtonVisibilityProperty); }
            set { SetValue(MinButtonVisibilityProperty, value); }
        }

        public static readonly DependencyProperty MaxButtonVisibilityProperty = DependencyProperty.Register(
            "MaxButtonVisibility", typeof (Visibility), typeof (AdvancedWindow), new PropertyMetadata(Visibility.Visible));

        public Visibility MaxButtonVisibility
        {
            get { return (Visibility) GetValue(MaxButtonVisibilityProperty); }
            set { SetValue(MaxButtonVisibilityProperty, value); }
        }

        public static readonly DependencyProperty ExitButtonVisibilityProperty = DependencyProperty.Register(
            "ExitButtonVisibility", typeof (Visibility), typeof (AdvancedWindow), new PropertyMetadata(Visibility.Visible));

        public Visibility ExitButtonVisibility
        {
            get { return (Visibility) GetValue(ExitButtonVisibilityProperty); }
            set { SetValue(ExitButtonVisibilityProperty, value); }
        }
    }
}
