﻿namespace Boss.BaseControls.Abstractions
{
    public interface IBusy
    {
        bool IsBusy { get; set; }
        string BusyContent { get; set; }
    }
}
