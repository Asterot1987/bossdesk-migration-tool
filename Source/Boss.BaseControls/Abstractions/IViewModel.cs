﻿using System.Threading.Tasks;

namespace Boss.BaseControls.Abstractions
{
    public interface IViewModel<T>
    {
        T InitData();
        void InitCommands();
    }
}
