﻿namespace Boss.BaseControls.Abstractions
{
    public interface ISplashScreen
    {
        void AddMessage(string message);
        void LoadComplete();
    }
}
