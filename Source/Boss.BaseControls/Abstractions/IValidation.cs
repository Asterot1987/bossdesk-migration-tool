﻿using Boss.BaseControls.Models;

namespace Boss.BaseControls.Abstractions
{
    public interface IValidation<T>
    {
        bool IsCorrect { get; set; }

        T ErrorData { get; set; }
    }
}
